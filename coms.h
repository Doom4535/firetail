/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FIRETAIL_COMS_INCLUDED
#define FIRETAIL_COMS_INCLUDED

#include "firetail.h"
#include "control.h"

class Coms : public Firelink::Vehicle
{
public:
    Coms();

    bool on_get_satellites(uint8_t* data);

    bool on_set_home_coordinates(uint8_t* data);
    bool on_get_home_coordinates(uint8_t* data);

    bool on_set_n_points(uint8_t* data);
    bool on_get_n_points(uint8_t* data);

    bool on_set_remove_point(uint8_t* data);

    bool on_set_clear_points(uint8_t* data);

    bool on_set_point(uint8_t* data);
    bool on_get_point(uint8_t* data);

    bool on_set_mag_var(uint8_t* data);
    bool on_get_mag_var(uint8_t* data);

    bool on_set_ahrs_cal(uint8_t* data);
    bool on_get_ahrs_cal(uint8_t* data);

    bool on_set_servo_config(uint8_t* data);
    bool on_get_servo_config(uint8_t* data);

    bool on_set_pnr_pid(uint8_t* data);
    bool on_get_pnr_pid(uint8_t* data);

    bool on_set_hna_pid(uint8_t* data);
    bool on_get_hna_pid(uint8_t* data);

    bool on_set_max_vs(uint8_t* data);
    bool on_get_max_vs(uint8_t* data);

    bool on_set_path_pid(uint8_t* data);
    bool on_get_path_pid(uint8_t* data);

    bool on_set_at_settings(uint8_t* data);
    bool on_get_at_settings(uint8_t* data);

    bool on_set_mixer_config(uint8_t* data);
    bool on_get_mixer_config(uint8_t* data);

    bool on_set_loiter_settings(uint8_t* data);
    bool on_get_loiter_settings(uint8_t* data);

    bool on_set_turn_coord(uint8_t* data);
    bool on_get_turn_coord(uint8_t* data);

    bool on_set_max_angles(uint8_t* data);
    bool on_get_max_angles(uint8_t* data);

    bool on_set_test_mode(uint8_t* data);

    bool on_set_engage_system(uint8_t* data);

    bool on_set_battery_mah(uint8_t* data);
    bool on_get_battery_mah(uint8_t* data);

    bool on_set_ap_mode(uint8_t* data);

    bool on_set_alarm_actions(uint8_t* data);
    bool on_get_alarm_actions(uint8_t* data);

    bool on_set_vh_stream_prio(uint8_t* data);
    bool on_get_vh_stream_prio(uint8_t* data);

    bool on_set_keep_alive(uint8_t* data);

    bool on_set_flight_command(uint8_t* data);

    bool on_set_mode_select_settings(uint8_t* data);
    bool on_get_mode_select_settings(uint8_t* data);

    bool on_set_channel_inversions(uint8_t* data);
	bool on_get_channel_inversions(uint8_t* data);

	bool on_set_skip_queue(uint8_t* data);
};

extern Coms coms;
extern Coms coms1;

#endif // FIRETAIL_COMS_INCLUDED
