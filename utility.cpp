#include "utility.h"

uint8_t read_bit(uint8_t bte, uint8_t n)
{
    bool on = bte & (1 << (n-1));

    return on;
}

uint8_t set_bit(uint8_t bte, uint8_t n, bool on)
{
    uint8_t ret = bte;
    if(on)
        ret |= 1 << (n-1);
    else
        ret &= ~(1 << (n-1));

    return ret;
}
