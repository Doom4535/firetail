/*
    FireTail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SD_CARD_MANAGER_H
#define SD_CARD_MANAGER_H

#include <Arduino.h>
#include "ChibiOS_ARM.h"
#include "SdFat.h"
#include "ahrs.h"
#include "airdata.h"
#include "config.h"
#include "settings.h"
#include "CompleteGPS.h"
#include "hil.h"
#include "navigation.h"

extern bool sd_card_ok;

/**
* checks the SD card and starts a log file
* possible return values
*	1 	= OK
*	-1	= bad card
*	-2	= bad format
*	-3 	= couldn't create log file
*/
int sd_card_init();
bool sd_card_start_logging();

void sd_card_save_plan(waypoint* wp, waypoint* gp);
void sd_card_load_plan(waypoint* wp, waypoint* gp);


#endif


