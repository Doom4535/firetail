/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hil.h"
#include "coms.h"

imu::Vector<3> hil_gyro;
imu::Vector<3> hil_acc;
imu::Vector<3> hil_mag;

imu::Vector<3> hil_orientation;

unsigned int hil_millis = 0;
unsigned int hil_micros = 0;
float hil_lat;
float hil_lng;
float hil_course;
float hil_speed;
float hil_alt;
float hil_last_alt;
float hil_dt;
float hil_vs = 0;

bool hil_mode_on = false;

float last_hil_us;

int buf_pos = 0;
char hil_buf[1024];


double white_noise()
{
    double temp1;
    double temp2;
    double result;
    int p;

    p = 1;

    while( p > 0 )
    {
        temp2 = ( rand() / ( (double)RAND_MAX ) );
        if ( temp2 == 0 )
            p = 1;
        else
            p = -1;
    }

    temp1 = cos( ( 2.0 * (double)PI ) * rand() / ( (double)RAND_MAX ) );
    result = sqrt( -2.0 * log( temp2 ) ) * temp1;
    return result;
}


unsigned long ms()
{
    if(hil_mode_on)
        return hil_millis;
    return millis();
}

unsigned long us()
{
    if(hil_mode_on)
        return hil_micros;
    return micros();
}

void hil_process()
{
    CompleteGPS::Tokeniser tok(hil_buf, ',');
    char item[128];
    memset(item, '\0', 128);
    int counter = 0;
    while(tok.next(item, 128))
    {
        switch(counter)
        {
        case 0:
        {
            float v = atof(item)*1000;
            if(v)
            {
                last_hil_us = hil_micros;
                hil_millis = v;
                hil_micros = v*1000;
                hil_dt = (last_hil_us - hil_micros) / 1000000.0;
            }
        }
        break;
        case 1:
        {
            float v = atof(item);
            if(v)
                hil_gyro.x() = v;
        }
        break;
        case 2:
        {
            float v = atof(item);
            if(v)
                hil_gyro.y() = v;
        }
        break;
        case 3:
        {
            float v = atof(item);
            if(v)
                hil_gyro.z() = v;
        }
        break;
        case 4:
        {
            float v = atof(item);
            if(v)
                hil_acc.x() = -v+(white_noise()*0.01);
        }
        break;
        case 5:
        {
            float v = atof(item);
            if(v)
                hil_acc.y() = -v+(white_noise()*0.01);
        }
        break;
        case 6:
        {
            float v = atof(item);
            if(v)
                hil_acc.z() = -v+(white_noise()*0.01);
        }
        break;
        case 7: //FlightGear doesn't do magnetic field vectors, so we have to make a fake one
        {
            //this isn't really ideal AT ALL, but it works
            float v = atof(item);
            if(v)
            {
                double heading = v+(white_noise()*0.05);
                heading = (heading+90) * 0.01745329251;
                hil_mag.x() = sin(heading);
                hil_mag.y() = cos(heading);
                hil_mag.z() = 0.0;
            }
        }
        break;
        case 8:
        {
            float v = atof(item);
            if(v)
                hil_lat = v+(white_noise()*0.000001);
        }
        break;
        case 9:
        {
            float v = atof(item);
            if(v)
                hil_lng = v+(white_noise()*0.000001);
        }
        break;
        case 10:
        {
            float v = atof(item);
            if(v)
                hil_course = v+(white_noise()*0.01);
        }
        break;
        case 11:
        {
            float v = atof(item);
            hil_speed = v*MS_TO_KMH;//+(white_noise()*0.05);
        }
        break;
        case 12:
        {
            float v = atof(item);
            if(v)
            {
                hil_last_alt = hil_alt;
                hil_alt = v;
                hil_vs = (v - hil_last_alt) / hil_dt;
                hil_vs *= FEET_TO_METERS;
            }
        }
        break;
        case 13:
        {
            hil_orientation.x() = atof(item)*100;
        }
        break;
        case 14:
        {
            hil_orientation.y() = atof(item)*100;
        }
        break;
        case 15:
        {
            hil_orientation.z() = atof(item)*100;
        }
        break;
        }
        counter++;
    }
}

void hil_encode(char c)
{
    if(c == '\n')
    {
        hil_process();
        memset(hil_buf, '\0', 1024);
        buf_pos = 0;
    }
    else
    {
        hil_buf[buf_pos] = c;
        buf_pos++;
        if(buf_pos >= 1024)
        {
            memset(hil_buf, '\0', 1024);
            buf_pos = 0;
        }
    }
}

void hil_write(int16_t* control_out)
{
    char outbuf[128];
    float co[4];
    int i;
    for(i = 0; i <= 3; i++)
        co[i] = control_out[i]/127.0;

    co[AILERON_CHANNEL] *= -1.0;
    co[THROTTLE_CHANNEL] = (control_out[THROTTLE_CHANNEL]+127)/127.0;
    snprintf(outbuf, 128, "%f,%f,%f,%f\n", co[AILERON_CHANNEL], co[ELEVATOR_CHANNEL], co[RUDDER_CHANNEL], co[THROTTLE_CHANNEL]);
    FG_PORT.print(outbuf);
}


