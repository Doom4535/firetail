/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Arduino.h>
#include "RCControl.h"
#include "filters.h"

volatile unsigned long chans_in[6];
volatile unsigned long ulStartPeriod[6];
volatile unsigned long last_interrupt = 0;

void calcElevator();
void calcAileron();
void calcRudder();
void calcThrottle();
void calcChan5();
void calcChan6();


void rc_control_setup()
{

    int i;
    for(i = 0; i < 6; i++)
    {
        chans_in[i] = 1500;
        ulStartPeriod[i] = 0;
    }

    pinMode(ELEVATOR_SIGNAL_IN, INPUT);
    pinMode(AILERON_SIGNAL_IN, INPUT);
    pinMode(RUDDER_SIGNAL_IN, INPUT);
    pinMode(THROTTLE_SIGNAL_IN, INPUT);
    pinMode(SERVO_5_IN, INPUT);
    pinMode(SERVO_6_IN, INPUT);

    attachInterrupt(ELEVATOR_SIGNAL_IN, calcElevator, CHANGE);
    attachInterrupt(AILERON_SIGNAL_IN, calcAileron, CHANGE);
    attachInterrupt(RUDDER_SIGNAL_IN, calcRudder, CHANGE);
    attachInterrupt(THROTTLE_SIGNAL_IN, calcThrottle, CHANGE);
    attachInterrupt(SERVO_5_IN, calcChan5, CHANGE);
    attachInterrupt(SERVO_6_IN, calcChan6, CHANGE);

    last_interrupt = micros();
}




volatile int rc_get_channel(int n)
{
    return chans_in[n];
}


bool rc_have_signal()
{
    return ((micros() - last_interrupt) < 1500000);
}

void calcElevator()
{
    last_interrupt = micros();

    if(digitalRead(ELEVATOR_SIGNAL_IN) == HIGH)
    {
        ulStartPeriod[ELEVATOR_CHANNEL] = micros();
    }
    else
    {
        if(ulStartPeriod[ELEVATOR_CHANNEL])
        {
            chans_in[ELEVATOR_CHANNEL] = (micros() - ulStartPeriod[ELEVATOR_CHANNEL]);
            ulStartPeriod[ELEVATOR_CHANNEL] = 0;
        }
    }
}

void calcAileron()
{
	last_interrupt = micros();

	if(digitalRead(AILERON_SIGNAL_IN) == HIGH)
    {
        ulStartPeriod[AILERON_CHANNEL] = micros();
    }
    else
    {
        if(ulStartPeriod[AILERON_CHANNEL])
        {
            chans_in[AILERON_CHANNEL] = (micros() - ulStartPeriod[AILERON_CHANNEL]);
            ulStartPeriod[AILERON_CHANNEL] = 0;
        }
    }
}

void calcRudder()
{
	last_interrupt = micros();

	if(digitalRead(RUDDER_SIGNAL_IN) == HIGH)
    {
        ulStartPeriod[RUDDER_CHANNEL] = micros();
    }
    else
    {
        if(ulStartPeriod[RUDDER_CHANNEL])
        {
            chans_in[RUDDER_CHANNEL] = (micros() - ulStartPeriod[RUDDER_CHANNEL]);
            ulStartPeriod[RUDDER_CHANNEL] = 0;
        }
    }
}

void calcThrottle()
{
	last_interrupt = micros();

	if(digitalRead(THROTTLE_SIGNAL_IN) == HIGH)
    {
        ulStartPeriod[THROTTLE_CHANNEL] = micros();
    }
    else
    {
        if(ulStartPeriod[THROTTLE_CHANNEL])
        {
            chans_in[THROTTLE_CHANNEL] = (micros() - ulStartPeriod[THROTTLE_CHANNEL]);
            ulStartPeriod[THROTTLE_CHANNEL] = 0;
        }
    }
}

void calcChan5()
{
	last_interrupt = micros();

	if(digitalRead(SERVO_5_IN) == HIGH)
    {
        ulStartPeriod[SERVO_5_CHANNEL] = micros();
    }
    else
    {
        if(ulStartPeriod[SERVO_5_CHANNEL])
        {
            chans_in[SERVO_5_CHANNEL] = (micros() - ulStartPeriod[SERVO_5_CHANNEL]);
            ulStartPeriod[SERVO_5_CHANNEL] = 0;
        }
    }

}

void calcChan6()
{
	last_interrupt = micros();

    if(digitalRead(SERVO_6_IN) == HIGH)
    {
        ulStartPeriod[SERVO_6_CHANNEL] = micros();
    }
    else
    {
        if(ulStartPeriod[SERVO_6_CHANNEL])
        {
            chans_in[SERVO_6_CHANNEL] = (micros() - ulStartPeriod[SERVO_6_CHANNEL]);
            ulStartPeriod[SERVO_6_CHANNEL] = 0;
        }
    }
}

