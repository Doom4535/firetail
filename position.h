/*
    FireTail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
 The primary positioning device is the GPS. Without that, we have no ability
 to determine our position accurately. The downside to GPS is that it isn't a
 fast sensor. Most units spit out NMEA at 10Hz or less.

 We can fill in the gaps by dead-reckoning for short periods. We know the
 cruise speed and we can use the gyros to update heading. Of course, wind
 speed and direction will effect this, so dead-reckoning is only useful
 for a maximum of about 20 seconds.
*/

#ifndef POSITION_H_INCLUDED
#define POSITION_H_INCLUDED

#include "navigation.h"

void pos_begin();
void pos_iterate();

coord pos_get();
float pos_get_speed();
float pos_get_track();
int pos_certainty();

#endif // POSITION_H_INCLUDED
