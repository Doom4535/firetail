/*
    Firetail UAV System

	autotune.h

    A relay method PID autotuner.
	Inspired by the work of Brett Beauregard and William Spinelli
	http://brettbeauregard.com/blog/2012/01/arduino-pid-autotune-library/

    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUTOTUNE_H_INCLUDED
#define AUTOTUNE_H_INCLUDED

class PIDTuner
{
public:
    PIDTuner();
    int Runtime();
    void Cancel();

    void SetOutputStep(float);
    float GetOutputStep();

    void SetControlType(int);
    int GetControlType();

    void SetLookbackSec(float);
    int GetLookbackSec();

    void SetNoiseBand(float);
    float GetNoiseBand();

    float GetKp();
    float GetKi();
    float GetKd();

private:
    void FinishUp();
    bool isMax, isMin;
    float *input, *output;
    float setpoint;
    float noiseBand;
    int controlType;
    bool running;
    unsigned long peak1, peak2, lastTime;
    int sampleTime;
    int nLookBack;
    int peakType;
    float lastInputs[101];
    float peaks[10];
    int peakCount;
    bool justchanged;
    bool justevaled;
    float absMax, absMin;
    float oStep;
    float outputStart;
    float Ku, Pu;

};

#endif

