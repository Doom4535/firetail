/*
    FireTail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AIR_DATA_H
#define AIR_DATA_H

#include <Arduino.h>

void ad_init();
void ad_iterate();
float ad_pressure_alt(); //referenced to 1013.25mb
float ad_vert_speed(); //feet per second
float ad_temp(); //degrees C
float ad_airspeed();
float ad_xwind();

#endif

