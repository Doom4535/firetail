/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "pid.h"
#include "config.h"


PIDController::PIDController()
{
    integral_constraint = previous_error = integral = Kp = Ki = Kd = windup_limit = float(0.0);
    der_filter = ExpoMovingAvg(0.2, 0);
    inverted = false;
}

float PIDController::step(float error, float dt)
{
    if((error < windup_limit) && (error > -windup_limit))
    {
        if(inverted)
            integral = constrain(integral - error*dt, float(-integral_constraint), float(integral_constraint));
        else
            integral = constrain(integral + error*dt, float(-integral_constraint), float(integral_constraint));

        if(fabs(integral*Ki) > integral_constraint)
            integral -= error*dt;
    }

    der_filter.step((error - previous_error)/dt);

    float output = Kp*error + Ki*integral + Kd*der_filter.get();
    previous_error = error;
    return output;
}

float PIDController::step(float setpoint, float measurement, float dt)
{
    float error = setpoint - measurement;

    integral += error*dt;
    integral = constrain(integral, -(integral_constraint/Ki), (integral_constraint/Ki));

    der_filter.step((error - previous_error)/dt);

    float output = Kp*error + Ki*integral + Kd*der_filter.get();
    previous_error = error;
    return output;
}

float CircularPIDController::step(float setpoint, float measurement, float dt)
{
    while(setpoint < float(-18000.0))
        setpoint += float(36000.0);
    while(setpoint > float(18000.0))
        setpoint -= float(36000.0);

    while(measurement < float(-18000.0))
        measurement += float(36000.0);
    while(measurement > float(18000.0))
        measurement -= float(36000.0);

    float error = setpoint - measurement;
    while(error < float(-18000.0))
        error += float(36000.0);
    while(error > float(18000.0))
        error -= float(36000.0);

    if((error < windup_limit) && (error > -windup_limit))
        integral = constrain(integral + error*dt, float(-integral_constraint), float(integral_constraint));

    der_filter.step((error - previous_error)/dt);

    float output = Kp*error + Ki*integral + Kd*der_filter.get();
    previous_error = error;

    return output;
}

