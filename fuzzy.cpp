/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This fuzzy logic code is used to approximate non-linear functions.
    In this application it is used to generate errors for PID control.

    Traditional PID control is strictly linear and symmetrical. Using fuzzy
    logic to generate the error can improve the performance of the controller.
    Some hysteresis can be created near zero to reduce hunting tendancies.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "fuzzy.h"

float FuzzyMember::get_dom(float crisp_in)
{
    if(pos == FUZZY_SET_POS_MID)
    {
        if(crisp_in < min)
            return 0;
        if(crisp_in > max)
            return 0;
    }

    float range = (max - min);
    float half_point = (min + (range*0.5f));

    if(pos == FUZZY_SET_POS_START)
    {
        if(crisp_in < half_point)
            return 1;
    }
    if(pos == FUZZY_SET_POS_END)
    {
        if(crisp_in >= half_point)
            return 1;
    }

    crisp_in -= half_point;
    crisp_in /= range;
    crisp_in *= 2;

    if(crisp_in < 0)
        crisp_in *= -1;

    if(crisp_in > 1)
        crisp_in = 1;

    return (1-crisp_in);
}


FuzzySet::FuzzySet()
{
    sets[0].set_position(FUZZY_SET_POS_START);
    sets[1].set_position(FUZZY_SET_POS_MID);
    sets[2].set_position(FUZZY_SET_POS_MID);
    sets[3].set_position(FUZZY_SET_POS_MID);
    sets[4].set_position(FUZZY_SET_POS_END);
}


float FuzzySet::crisp_out(float crisp_in)
{
    float crisp_out = 0;
    for(uint8_t i = 0; i < 5; i++)
        crisp_out += sets[i].get_result(crisp_in);

    return crisp_out;
}

