/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HIL_H
#define HIL_H

#include "CompleteGPS.h"
#include "imumaths/imumaths.h"
#include "config.h"
#include "conversions.h"

void hil_encode(char c);
void hil_write(int16_t* out);

unsigned long ms();
unsigned long us();

extern imu::Vector<3> hil_gyro;
extern imu::Vector<3> hil_acc;
extern imu::Vector<3> hil_mag;
extern imu::Vector<3> hil_orientation;

extern unsigned int hil_millis;
extern float hil_lat; //degrees
extern float hil_lng; //degrees
extern float hil_course; //degrees true north
extern float hil_speed; //km/h
extern float hil_alt; //feet
extern float hil_vs;

extern bool hil_mode_on;

#endif

