/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FIRETAIL_H
#define FIRETAIL_H

#include <Arduino.h>
#include <ChibiOS_ARM.h>
#include "FireLink/vehicle.h"
#include "pid.h"
#include "fuzzy.h"
#include "Servo.h"
#include "RCControl.h"
#include "ahrs.h"
#include "airdata.h"
#include "CompleteGPS.h"
#include "navigation.h"
#include "position.h"
#include "settings.h"
#include "logger.h"
#include "hil.h"
#include "sensors/current_sensor.h"


#define CHAR_TO_SERVO_POS 0.703125f //scale for turning +/-128 to +/-90, so that control_out can be turned into a servo position

void firetail_begin();
void firetail_iterate();
void mainThread(); //main ChibiOS thread

extern Servo servos[6];

extern bool gps_initialised;

extern int delta_t;

extern waypoint home;

extern uint32_t keep_alive_seconds;
extern uint32_t last_pack_millis;

extern float heading_control_rate;
extern float altitude_control_rate;

extern float mag_var;

extern coord loiter_pos;

extern float selected_alt;
extern int16_t selected_roll;
extern int16_t selected_pitch;


extern waypoint points[512];
extern int current_waypoint;
extern waypoint from_point;
extern float xtrack_integral;
extern float last_xtrack_dist;
extern ExpoMovingAvg xtrack_der;
extern bool wp_hit;

extern bool rtb_mode;

extern waypoint geopoints[512];

extern float mahs_used;
extern uint32_t init_mah;


extern Firelink::ALARM_ACTION last_action;
extern bool boundary_alarm;
extern bool low_bat_alarm;
extern bool no_gps_alarm;
extern bool no_datalink_alarm;
extern bool no_signal_alarm;

extern ExpoMovingAvg gps_acceleration;
extern float last_speed;

extern Firelink::vs_inst inst;
extern Firelink::vs_systems systems;
extern Firelink::vs_ap_status ap_status;
extern Firelink::vs_raw_marg raw_marg;
extern Firelink::vs_raw_pwm raw_pwm;

extern bool motor_on;
extern bool autothrottle_on;
extern bool autolaunch_on;

#endif

