/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CONVERSIONS_H
#define CONVERSIONS_H

#include "core/wiring.h" //this file has DEG_TO_RAD and RAD_TO_DEG

#define KNOTS_TO_KMH 		1.852
#define KMH_TO_KNOTS        0.539957
#define KMH_TO_MS 			0.277777778
#define MS_TO_KMH 			3.6
#define MS_TO_KNOTS         1.94384449

#define METERS_TO_FEET 		3.2808399
#define FEET_TO_METERS 		0.3048

#define G_TO_MSS 			9.80665
#define MSS_TO_G 			0.101971621

#define FPS_TO_KMH          1.09728

#endif


