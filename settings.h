/*
    FireTail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SETTINGS_H
#define SETTINGS_H

#include <stdint.h>
#include <math.h>
#include "imumaths/vector.h"
#include "FireLink/types.h"
#include "EEPROM.h"

//this struct contains every parameter that should be stored to EEPROM
struct _config
{
    struct _ahrs_offset
    {
        float w, x, y, z;
    };

    _ahrs_offset ahrs_offset;

    struct _pid
    {
        float kp, ki, kd;
    };

    _pid pitch_pid;
    _pid roll_pid;
    _pid vs_pid;
    _pid heading_pid;
    _pid alt_pid;
    _pid path_pid;

    uint16_t mag_bias_x;
    uint16_t mag_bias_y;
    uint16_t mag_bias_z;
    float mag_gain_x;
    float mag_gain_y;
    float mag_gain_z;
    float accel_bias_x;
    float accel_bias_y;
    float accel_bias_z;

    float servo_signal_scale;
    int8_t elevator_min;
    int8_t elevator_max;
    int8_t aileron_min;
    int8_t aileron_max;
    int8_t rudder_min;
    int8_t rudder_max;
    int8_t throttle_min;
    int8_t throttle_max;

    Firelink::MIXER_CONFIGURATION mixer_config;
    int8_t mixer_ratio;

    uint16_t loiter_radius;
    float loiter_correction;

    _pid autothrottle;

    float at_settings_pt;
    float at_cruise_speed;
    float at_cruise_power;
    int8_t min_thr_setting;

    int16_t max_pitch_up;
    int16_t max_pitch_down;
    int16_t max_bank_angle;

    float yaw_coupling;
    float pitch_coupling;

    Firelink::ALARM_ACTION low_bat_action;
    Firelink::ALARM_ACTION boundary_action;
    Firelink::ALARM_ACTION no_gps_action;
    Firelink::ALARM_ACTION lost_control_action;
    Firelink::ALARM_ACTION no_telem_action;

    uint16_t max_pitch_rate;
    uint16_t max_roll_rate;

    uint16_t max_climb_rate; //feet per second
    uint16_t max_decent_rate;

    Firelink::AP_MODE mode_select_mode;
    uint8_t mode_select_channel;

    uint8_t input_channel_invert;
    uint8_t autopilot_channel_invert;
    uint8_t output_channel_invert;
};


int write_settings();
int read_settings();

extern _config settings;

#endif

