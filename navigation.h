/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NAVIGATION_H
#define NAVIGATION_H

#include <math.h>
#include <stdint.h>


struct coord
{
    float lat, lng;
};

class waypoint
{
public:
    waypoint() {
        used = false;
    }

    coord pos;
    int alt;
    bool used;
};


coord make_coord(int32_t lat, int32_t lng); //converts lat and lng expressed in millions of degrees
//to a coord struct that has floating point numbers

coord make_coordf(float lat, float lng);

float bearing_from_to(coord from, coord to);

float distance_between(coord a, coord b);

float cross_track_distance(coord from, coord to, coord here);

coord destination_from_distance_bearing(coord from, float dist, float bearing);



#endif

