/*
    FireTail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "airdata.h"
#include "filters.h"
#include "config.h"
#include "hil.h"
#include "ahrs.h"
#include "CompleteGPS.h"
#include "filters.h"
#include "sensors/LPS.h"
#include "coms.h"

static LPS lps25h;

static BrownLinearExpo raw_alt(0.9, 500);
static ExpoMovingAvg temp(0.1, 20.0);
static ExpoMovingAvg airspeed(0.05, 0);
static ExpoMovingAvg gps_vs(0.2, 0);

static scalarLinearKalman vs(0.3, 0, 0.0005, 0.001, 0.8);
static scalarLinearKalman alt(0.5, 500, 0.3, 0.2, 0.8);


static imu::Vector<3> S1;
static imu::Vector<3> F1;

static imu::Vector<2> wind_vector;

static float last_alt = 0;
static float xwind = 0;

static unsigned long last_us = 0;

void ad_generate_airspeed();
void ad_generate_wind_vector();

void xwind_calculate();


void ad_init()
{
    lps25h.init(LPS::device_25H, LPS::sa0_high);
    lps25h.enableDefault();
    last_us = us();

    raw_alt = BrownLinearExpo(0.1, lps25h.pressureToAltitudeFeet(lps25h.readPressureInchesHg()));
}


void ad_iterate()
{
    float dt = us()-last_us;
    if(dt < 10000)
        return;
    last_us = us();
    dt /= 1000000.0;

    float raw_press_alt = lps25h.pressureToAltitudeFeet(lps25h.readPressureInchesHg());

    if(hil_mode_on)
        raw_alt.step(hil_alt);
    else
        raw_alt.step(raw_press_alt);

    //obtain vertical acceleration and convert it to feet/s/s
    imu::Vector<3> wa = ahrs_world_acceleration();
    float v_acc = (wa.z()-9.8)*METERS_TO_FEET;

    //derive vertical speed from rate of change of altitude
    float ba_vs = constrain(((raw_alt.get()-last_alt)/dt), -1000, 1000); //constrained to +/- 1000ft because that will never happen within 0.01 seconds
    //- stops the kalman going crazy when switched to HIL mode

    last_alt = raw_alt.get();

/*
    //then use a kalman filter to combine acceleration and vert. speed from baro alt
    //to whole point here is to get a more responsive vertical speed measurement without sacrificing too much low-pass smoothing
    if((gps.fix == CompleteGPS::FIX_3D) && (!hil_mode_on))
    {
        if((millis()-gps.alt_age) < 250)
        {
            gps_vs.step(gps.vert_speed*METERS_TO_FEET);
            if(fabs(gps_vs.get()-ba_vs) < 3)
                ba_vs = (ba_vs*0.7) + (gps_vs.get()*0.3);
        }
    }
*/
    vs.step(v_acc*dt, ba_vs); //combine acceleration with barometric alt

    //and since we've now got a pretty good vertical speed measurement
    //might as well use a kalman filter to get good responsive altitude estimate as well
    alt.step(vs.get_state()*dt, raw_alt.get());

    temp.step(lps25h.readTemperatureC());

    //ad_generate_airspeed();
    //xwind_calculate();
}



void ad_generate_airspeed()
{
    xwind_calculate();

    float theta, mag, vert_spd;

    if(hil_mode_on)
    {
        theta = hil_course*DEG_TO_RAD;
        mag = hil_speed*KMH_TO_MS;
        vert_spd = hil_vs/100.0;
    }
    else
    {
        theta = gps.course/100.0*DEG_TO_RAD;
        mag = gps.speed/100.0*KMH_TO_MS;
        vert_spd = gps.vert_speed/100.0;
    }

    imu::Vector<3> S2;
    S2.x() = mag*cos(theta);
    S2.y() = mag*sin(theta);
    S2.z() = 0.0;

    imu::Vector<3> F2 = ahrs_get_matrix().row_to_vector(0);

    imu::Vector<3> dS = (S2-S1);
    imu::Vector<3> dF = (F2-F1);

    if((dF.magnitude() < 0.3) && (dF.magnitude() > 0.02))
    {
        float aspd = (dS.magnitude()/dF.magnitude());
        airspeed.step(aspd);
    }

    ad_generate_wind_vector();

    S1 = S2;
    F1 = F2;
}

void ad_generate_wind_vector()
{
    imu::Vector<2> wv;

    float headwind = (systems.ground_speed/100.0) - airspeed.get()*MS_TO_KMH;

    wv.x() = headwind;
    wv.y() = xwind;

    wind_vector = (wind_vector*0.75)+(wv*0.25);
    if(isnan(wind_vector.magnitude()) || isinf(wind_vector.magnitude()))
        wind_vector = imu::Vector<2>(0, 0);
}


void xwind_calculate()
{
    float theta = (systems.ground_track/100.0) - (inst.heading/100.0);
    xwind = sin(theta*DEG_TO_RAD)*(systems.ground_speed/100.0); //sin for cross-wind, cos for forward ground speed
}

float ad_pressure_alt()
{
    return alt.get_state();
}

float ad_vert_speed()
{
    return vs.get_state();
}

float ad_temp()
{
    return temp.get();
}

float ad_airspeed()
{
    return airspeed.get();
}

imu::Vector<2> ad_wind_vector()
{
    return wind_vector;
}

float ad_xwind()
{
    return xwind;
}
