/*
    Inertial Measurement Unit Maths Library
    Copyright (C) 2013-2014  Samuel Cowen
	www.camelsoftware.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IMUMATH_VECTOR_HPP
#define IMUMATH_VECTOR_HPP

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#define IMU_NO_MALLOC
#define IMU_VECTOR_SIZE 3
#define IMU_MATRIX_WIDTH 3


namespace imu
{

template <uint8_t N> class Vector
{
public:
	Vector()
	{
	    #ifndef IMU_NO_MALLOC
		p_vec = (float*)malloc(sizeof(float)*N);
        #endif
        for(int i = 0; i < N; i++)
            p_vec[i] = 0.0f;
	}

	Vector(float a)
	{
		#ifndef IMU_NO_MALLOC
		p_vec = (float*)malloc(sizeof(float)*N);
        #endif
        p_vec[0] = a;
	}

	Vector(float a, float b)
	{
		#ifndef IMU_NO_MALLOC
		p_vec = (float*)malloc(sizeof(float)*N);
        #endif
        p_vec[0] = a;
        p_vec[1] = b;
	}

	Vector(float a, float b, float c)
	{
		#ifndef IMU_NO_MALLOC
		p_vec = (float*)malloc(sizeof(float)*N);
        #endif
        p_vec[0] = a;
        p_vec[1] = b;
        p_vec[2] = c;
	}

    Vector(float a, float b, float c, float d)
    {
		#ifndef IMU_NO_MALLOC
		p_vec = (float*)malloc(sizeof(float)*N);
        #endif
        for(int i = 0; i < N; i++)
            p_vec[i] = 0.0f;

        p_vec[0] = a;
        p_vec[1] = b;
        p_vec[2] = c;
        p_vec[3] = d;
    }

    Vector(const Vector<N> &v)
    {
        #ifndef IMU_NO_MALLOC
		p_vec = (float*)malloc(sizeof(float)*N);
        #endif

        for (int x = 0; x < N; x++ )
            p_vec[x] = v.p_vec[x];
    }

    ~Vector()
    {
        #ifndef IMU_NO_MALLOC
        free(p_vec);
        #endif
    }

    uint8_t n() { return N; }

    //returns length of vector
    float magnitude()
    {
        float res = 0;
        int i;
        for(i = 0; i < N; i++)
            res += (p_vec[i] * p_vec[i]);

        if(res != 1.0) //avoid a sqrt if possible
            return sqrt(res);
        return 1;
    }

    //returns angle between x and y dimensions
    float theta()
    {
        if(x() != 0.0f)
            return atan(y()/x());
        return 0;
    }

    //sets magnitude to 1.0
    void normalize()
    {
        float mag = magnitude();
        if(mag == float(0))
            return;

        int i;
        for(i = 0; i < N; i++)
            p_vec[i] = p_vec[i]/mag;
    }

    //returns the dot product of two vectors
    float dot(Vector v)
    {
        float ret = 0;
        int i;
        for(i = 0; i < N; i++)
            ret += p_vec[i] * v.p_vec[i];

        return ret;
    }

    //generates the cross product of two 3D vectors
    Vector cross(Vector v)
    {
        Vector ret;

         //the cross product is only valid for vectors with 3 dimensions,
         //with the exception of higher dimensional stuff that is beyond the intended scope of this library
        if(N != 3)
            return ret;

        ret.p_vec[0] = (p_vec[1] * v.p_vec[2]) - (p_vec[2] * v.p_vec[1]);
        ret.p_vec[1] = (p_vec[2] * v.p_vec[0]) - (p_vec[0] * v.p_vec[2]);
        ret.p_vec[2] = (p_vec[0] * v.p_vec[1]) - (p_vec[1] * v.p_vec[0]);
        return ret;
    }

    //scales a vector. this changes the magnitude only
    Vector scale(float scalar)
    {
        Vector ret;
        for(int i = 0; i < N; i++)
            ret.p_vec[i] = p_vec[i] * scalar;
        return ret;
    }

    //inverts
    Vector invert()
    {
        Vector ret;
        for(int i = 0; i < N; i++)
            ret.p_vec[i] = -p_vec[i];
        return ret;
    }

    Vector operator = (Vector v)
    {
        for (int x = 0; x < N; x++ )
            p_vec[x] = v.p_vec[x];
		return *this;
    }

    float& operator [](int n)
    {
        return p_vec[n];
    }

    float& operator ()(int n)
    {
        return p_vec[n];
    }

    Vector operator + (Vector v)
    {
        Vector ret;
        for(int i = 0; i < N; i++)
            ret.p_vec[i] = p_vec[i] + v.p_vec[i];
        return ret;
    }

    Vector operator - (Vector v)
    {
        Vector ret;
        for(int i = 0; i < N; i++)
            ret.p_vec[i] = p_vec[i] - v.p_vec[i];
        return ret;
    }

    Vector operator * (float scalar)
    {
        return scale(scalar);
    }

    Vector operator / (float scalar)
    {
        Vector ret;
        for(int i = 0; i < N; i++)
            ret.p_vec[i] = p_vec[i] / scalar;
        return ret;
    }

	Vector operator += (Vector v)
	{
		*this = *this + v;
	}

    void toDegrees()
    {
        for(int i = 0; i < N; i++)
            p_vec[i] *= 57.2957795131; //180/pi
    }

    void toRadians()
    {
        for(int i = 0; i < N; i++)
            p_vec[i] *= 0.01745329251;  //pi/180
    }

    float& x() { return p_vec[0]; }
    float& y() { return p_vec[1]; }
    float& z() { return p_vec[2]; }


private:
    #ifndef IMU_NO_MALLOC
    float* p_vec;
    #else
    float p_vec[IMU_VECTOR_SIZE];
	#endif
};


};

#endif
