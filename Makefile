
# The name of your project (used to name the compiled .hex file)
TARGET = firetail

TEENSYPATH = /home/samuel/Programming/Teensy/IDE

# configurable options
OPTIONS = -DF_CPU=96000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH

# options needed by many Arduino libraries to configure for Teensy 3.0
OPTIONS += -D__MK20DX256__ -DARDUIO=104 #-ffunction-sections -fdata-sections


#************************************************************************
# Location of Teensyduino utilities, Toolchain, and Arduino Libraries.
# To use this makefile without Arduino, copy the resources from these
# locations and edit the pathnames.  The rest of Arduino is not needed.
#************************************************************************

# path location for Teensy Loader, teensy_post_compile and teensy_reboot
TOOLSPATH = $(TEENSYPATH)/hardware/tools   # on Linux
#TOOLSPATH = ../../../tools/avr/bin   # on Mac or Windows

# path location for the arm-none-eabi compiler
COMPILERPATH = $(TEENSYPATH)/hardware/tools/arm-none-eabi/bin

#************************************************************************
# Settings below this point usually do not need to be edited
#************************************************************************

INCLUDEPATHS = -I./core -I./ChibiOS -I./SdFat -I./Squirrel/include
# CPPFLAGS = compiler options for C and C++
CPPFLAGS = $(INCLUDEPATHS) -Wall -g -Os -mcpu=cortex-m4 -mthumb -nostdlib -MMD $(OPTIONS) -I.

# compiler options for C++ only
CXXFLAGS = -std=gnu++0x -felide-constructors -fno-exceptions -fno-rtti

# compiler options for C only
CFLAGS = 

# linker options
LDFLAGS = -Os -Wl,--gc-sections -mcpu=cortex-m4 -mthumb -Tmk20dx256.ld

# additional libraries to link
LIBS = -lm


# names for the compiler programs
CC = $(abspath $(COMPILERPATH))/arm-none-eabi-gcc
CXX = $(abspath $(COMPILERPATH))/arm-none-eabi-g++
OBJCOPY = $(abspath $(COMPILERPATH))/arm-none-eabi-objcopy
SIZE = $(abspath $(COMPILERPATH))/arm-none-eabi-size

# automatically create lists of the sources and objects
# TODO: this does not handle Arduino libraries yet...
TEENSYCORE_C_FILES := $(wildcard core/*.c) 
TEENSYCORE_CPP_FILES := $(wildcard core/*.cpp)
FIRELINK_C_FILES := $(wildcard FireLink/*.c)
FIRELINK_CPP_FILES := $(wildcard FireLink/*.cpp)
SENSOR_CPP_FILES := $(wildcard sensors/*.cpp)
CHIBIOS_FILES := $(wildcard ChibiOS/*.c) $(wildcard ChibiOS/utility/*.c)
SDFAT_FILES := $(wildcard SdFat/*.cpp)
SQUIRREL_FILES := $(wildcard Squirrel/*.cpp)
C_FILES := $(wildcard *.c)
CPP_FILES := $(wildcard *.cpp)
OBJS := $(TEENSYCORE_C_FILES:.c=.o) $(TEENSYCORE_CPP_FILES:.cpp=.o) \
			$(FIRELINK_C_FILES:.c=.o) $(FIRELINK_CPP_FILES:.cpp=.o) \
			$(SENSOR_CPP_FILES:.cpp=.o) \
			$(CHIBIOS_FILES:.c=.o) $(SDFAT_FILES:.cpp=.o) \
			$(SQUIRREL_FILES:.cpp=.o) \
			$(C_FILES:.c=.o) $(CPP_FILES:.cpp=.o) 


# the actual makefile rules (all .o files built by GNU make's default implicit rules)

all: $(TARGET).hex

$(TARGET).elf: $(OBJS) mk20dx256.ld
	$(CC) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

%.hex: %.elf
	$(SIZE) $<
	$(OBJCOPY) -O ihex -R .eeprom $< $@
	#./teensy_loader -mmcu=mk20dx128 -w -v $(TARGET).hex
	$(abspath $(TOOLSPATH))/teensy_post_compile -file=$(basename $@) -path=$(shell pwd) -tools=$(abspath $(TOOLSPATH))
	-$(abspath $(TOOLSPATH))/teensy_reboot

upload: all
	./teensy_loader -mmcu=mk20dx128 -w -v $(TARGET).hex

monitor:
	screen /dev/ttyACM0 19200

monitor_debug:
	screen /dev/ttyACM1 115200

# compiler generated dependency info
-include $(OBJS:.o=.d)

clean:
	find ./ -type f -name "*.o" -delete
	find ./ -type f -name "*.d" -delete
	rm -f $(TARGET).elf $(TARGET).hex

cleanall: clean



