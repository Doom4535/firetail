/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FILTERS_H_INCLUDED
#define FILTERS_H_INCLUDED

#include "imumaths/imumaths.h"

class ExpoMovingAvg
{
public:
    ExpoMovingAvg()
    {
        a = 0.5;
    }
    ExpoMovingAvg(float f, float init_est = 0)
    {
        a = f;
        accumulator = init_est;
    }

    void set_factor(float factor)
    {
        a = factor;
    }
    void step(float measurement)
    {
        accumulator = (a* measurement) + (1.0 - a) * accumulator;
    }

    float get()
    {
        return accumulator;
    }

private:
    float accumulator;
    float a;
};

class CircularExpoMovingAvg
{
public:
    CircularExpoMovingAvg()
    {
        a = 0.5;
    }
    CircularExpoMovingAvg(float f, float init_est = 0)
    {
        a = f;
        accumulator = init_est;
    }

    void set_factor(float factor)
    {
        a = factor;
    }
    void step(float measurement)
    {
        while(measurement > 18000)
            measurement -= 36000;
        while(measurement < -18000)
            measurement += 36000;

        accumulator = (a* measurement) + (1.0 - a) * accumulator;

        while(accumulator > 18000)
            accumulator -= 36000;
        while(accumulator < -18000)
            accumulator += 36000;
    }

    float get()
    {
        return accumulator;
    }

private:
    float accumulator;
    float a;
};


class BrownLinearExpo
{
public:
    BrownLinearExpo()
    {
        a = 0.5;
    }
    BrownLinearExpo(float f, float init_est)
    {
        a = f;
        estimate = init_est;
    }

    void set_factor(float factor)
    {
        a = factor;
    }
    void step(float measurement)
    {
        single_smoothed = a * measurement + (1 - a) * single_smoothed;
        double_smoothed = a * single_smoothed + (1 - a) * double_smoothed;

        float est_a = (2*single_smoothed - double_smoothed);
        float est_b = (a / (1-a) )*(single_smoothed - double_smoothed);
        estimate = est_a + est_b;
    }
    float get()
    {
        return estimate;
    }

private:
    float estimate, double_smoothed, single_smoothed;
    float a;
};



class scalarLinearKalman
{
public:
    scalarLinearKalman(double control_gain, double initial_state_estimate, double initial_covariance, float control_noise, float measurement_noise)
    {
        B = control_gain;
        current_state_estimate = initial_state_estimate;
        current_prob_estimate = initial_covariance;
        Q = control_noise;
        R = measurement_noise;
    }

    double get_state()
    {
        return current_state_estimate;
    }


    void step(double control_vector, double measurement_vector)
    {
        //prediction
        double predicted_state_estimate = (current_state_estimate) + (B * control_vector);
        double predicted_prob_estimate = current_prob_estimate + Q;

        //observation
        double innovation = measurement_vector - predicted_state_estimate;
        double innovation_covariance = predicted_prob_estimate + R;

        //update
        double kalman_gain = predicted_prob_estimate * innovation_covariance;
        current_state_estimate = predicted_state_estimate + kalman_gain * innovation;
        current_prob_estimate = (1 - kalman_gain) * predicted_prob_estimate;
    }

    double B, current_state_estimate, current_prob_estimate;
    float Q, R;
};



#endif
