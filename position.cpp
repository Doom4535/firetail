/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "position.h"
#include "coms.h"

#include "CompleteGPS.h"
#include "ahrs.h"
#include "airdata.h"
#include "filters.h"
#include "navigation.h"
#include "hil.h"
#include "conversions.h"
#include "settings.h"
#include "firetail.h"

static coord pos_here;

static imu::Vector<2> velocity;

static float last_micros;

static float dead_speed; //dead reckoned ground speed
static float dead_course; //dead reckoned course
static float last_heading;

static float last_dist;

static float pos_ahrs_velocity;

static unsigned long last_gps_pos_age;

void pos_begin()
{
    last_micros = us();
    last_dist = 0;

    pos_here.lat = pos_here.lng = 0;
}

float pos_get_speed()
{
    return dead_speed*MS_TO_KMH;
}
float pos_get_track()
{
    return dead_course;
}

void pos_iterate()
{
    if(!hil_mode_on)
    {
        if(!gps_initialised)
        {
            pos_here.lat = gps.latitude;
            pos_here.lng = gps.longitude;
            return;
        }

        if(systems.gps_lock >= Firelink::GPS_LOCK_2D) //if the GPS has at least a 2D lock
        {
            //ignore big jumps if gps has initialised
            if(gps.latlng_age != last_gps_pos_age)
            {
                last_gps_pos_age = gps.latlng_age;
                coord there = make_coordf(gps.latitude, gps.longitude);
                if(distance_between(pos_here, there) < 100000)
                {

                    pos_here.lat = gps.latitude;
                    pos_here.lng = gps.longitude;
                }
            }

            dead_speed = gps.speed*KMH_TO_MS*0.01;
            dead_course = gps.course/100.0;
            return;
        }
    }
    else
    {
        pos_here.lat = hil_lat;
        pos_here.lng = hil_lng;

        dead_speed = hil_speed*KMH_TO_MS;
        dead_course = hil_course;
        return;
    }


    //this code will only run when the GPS does not have a position fix

    float hdg = ahrs_get_euler().x();
    //eventually it guesses it's at cruise speed. without a GPS the autothrottle selects cruise power,
    //so this is a reasonable assumption
    dead_speed = (dead_speed*0.999) + ((settings.at_cruise_speed*0.001)*KMH_TO_MS);

    //use rate of change of heading from ahrs to update course
    //hopefully the magnetometers are reasonably accurate
    dead_course += (hdg - last_heading);
    dead_course = (dead_course*0.999) + (hdg*0.001);

    last_heading = hdg;

    //keep dead-reckoned course within +/- 180 degrees
    while(dead_course > 180.0)
        dead_course -= 360.0;
    while(dead_course < -180.0)
        dead_course += 360.0;

    //calculate delta time
    float dt;
    dt = us() - last_micros;
    dt /= 1000000.0;
    last_micros = us();

    float dist = dead_speed*dt;
    last_dist += dist;
    //this navigation function loses accuracy if the distance is too small
    //so we only update position if the distance travelled is more than 5m
    if(last_dist > 5.0)
    {
        //then update position based on the above dead-reckoned information
        pos_here = destination_from_distance_bearing(pos_here, last_dist, dead_course);
        last_dist = 0;
    }
}

coord pos_get()
{
    return pos_here;
}

int pos_certainty()
{
    //TODO
}
