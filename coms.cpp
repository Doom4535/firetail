#include "coms.h"
#include "ahrs.h"


Coms::Coms()
{
    set_v_stream_ptr(Firelink::VS_INST, (uint8_t*)&inst);
    set_v_stream_ptr(Firelink::VS_SYSTEMS,(uint8_t*) &systems);
    set_v_stream_ptr(Firelink::VS_AP_STATUS, (uint8_t*)&ap_status);
    set_v_stream_ptr(Firelink::VS_RAW_MARG, (uint8_t*)&raw_marg);
    set_v_stream_ptr(Firelink::VS_RAW_PWM, (uint8_t*)&raw_pwm);
}
/*
THE FOLLOWING FUNCTIONS ARE FIRELINK CALLBACK FUNCTIONS.

These functions are called when the communications loop reads a command from
the ground control station.

*/

bool Coms::on_get_satellites(uint8_t* data)
{
    uint16_t sat_num = (uint16_t)*data;
    Firelink::m_satellites msat;
    msat.azimuth = gps.sats[sat_num].azimuth;
    msat.elevation = gps.sats[sat_num].elevation;
    msat.prn = gps.sats[sat_num].prn;
    msat.ss = gps.sats[sat_num].snr;
    msat.sat_num = sat_num;
    send_satellites(&msat);
    return true;
}

/**
Sends the home base coordinates to the station.
*/
bool Coms::on_get_home_coordinates(uint8_t* data)
{
    Firelink::m_home_coordinates hc;
    hc.lat = home.pos.lat*1000000;
    hc.lng = home.pos.lng*1000000;
    hc.alt = home.alt;

    send_home_coordinates(&hc);
    return true;
}

/**
The station wants to set home base coordinates and elevation.
*/
bool Coms::on_set_home_coordinates(uint8_t* data)
{
    Firelink::m_home_coordinates* hc = (Firelink::m_home_coordinates*)data;
    home.pos.lat = hc->lat/1000000.0;
    home.pos.lng = hc->lng/1000000.0;
    home.alt = hc->alt;
    return true;
}


bool Coms::on_set_n_points(uint8_t* data)
{
    return true;
}

bool Coms::on_set_clear_points(uint8_t* data)
{
    Firelink::m_clear_points* cp = (Firelink::m_clear_points*)data;
    if(cp->point_type == Firelink::POINT_TYPE_WAYPOINT)
    {
        for(int i = 0; i < 512; i++)
            points[i].used = false;
    }
    if(cp->point_type == Firelink::POINT_TYPE_GEOPOINT)
    {
        for(int i = 0; i < 512; i++)
            geopoints[i].used = false;
    }
    return true;
}

bool Coms::on_get_n_points(uint8_t* data)
{
    uint16_t n = 0;
    while((points[n].used) && (n != 512))
        n++;

    Firelink::m_n_points np;
    np.wp = n;

    n = 0;
    while((geopoints[n].used) && (n != 512))
        n++;
    np.gp = n;

    send_n_points(&np);
    return true;
}

/**
Removes a waypoint - it actually just reshuffles the array.
*/
bool Coms::on_set_remove_point(uint8_t* data)
{
    Firelink::m_remove_point* rmp = (Firelink::m_remove_point*)data;
    if(rmp->n > 512)
        return true;

    if(rmp->point_type == Firelink::POINT_TYPE_WAYPOINT)
    {
        points[rmp->n].used = false;
        for(unsigned int i = rmp->n+1; i < 512; i++)
            points[i-1] = points[i];
    }
    if(rmp->point_type == Firelink::POINT_TYPE_GEOPOINT)
    {
        geopoints[rmp->n].used = false;
        for(unsigned int i = rmp->n+1; i < 512; i++)
            points[i-1] = points[i];
    }

    sd_card_save_plan(points, geopoints);
    return true;
}

bool Coms::on_set_point(uint8_t* data)
{
    Firelink::m_point* pt = (Firelink::m_point*)data;
    if(pt->n > 512)
        return true;

    if(pt->type == Firelink::POINT_TYPE_WAYPOINT)
    {
        points[pt->n].used = true;
        points[pt->n].pos.lat = pt->lat/1000000.0;
        points[pt->n].pos.lng = pt->lng/1000000.0;
        points[pt->n].alt = pt->alt;
    }
    if(pt->type == Firelink::POINT_TYPE_GEOPOINT)
    {
        geopoints[pt->n].used = true;
        geopoints[pt->n].pos.lat = pt->lat/1000000.0;
        geopoints[pt->n].pos.lng = pt->lng/1000000.0;
        geopoints[pt->n].alt = pt->alt;
    }

    sd_card_save_plan(points, geopoints);
    return true;
}

bool Coms::on_get_point(uint8_t* data)
{
    Firelink::m_point* ps = (Firelink::m_point*)data;
    Firelink::m_point pt;

    if(ps->n >= 512)
        return true;

    pt.type = ps->type;
    pt.n = ps->n;

    if(pt.type == Firelink::POINT_TYPE_WAYPOINT)
    {
        pt.lat = points[pt.n].pos.lat*1000000;
        pt.lng = points[pt.n].pos.lng*1000000;
        pt.alt = points[pt.n].alt;
    }
    if(pt.type == Firelink::POINT_TYPE_GEOPOINT)
    {
        pt.lat = geopoints[pt.n].pos.lat*1000000;
        pt.lng = geopoints[pt.n].pos.lng*1000000;
        pt.alt = geopoints[pt.n].alt;
    }

    send_point(&pt);

    return true;
}


bool Coms::on_set_mag_var(uint8_t* data)
{
    Firelink::m_mag_var* mv = (Firelink::m_mag_var*)data;
    mag_var = mv->n;
    return true;
}

bool Coms::on_get_mag_var(uint8_t* data)
{
    Firelink::m_mag_var mv;
    mv.n = mag_var;
    send_mag_var(&mv);
    return true;
}

bool Coms::on_set_ahrs_cal(uint8_t* data)
{
    Firelink::m_ahrs_cal* ac = (Firelink::m_ahrs_cal*)data;
    imu::Vector<3> gain;
    imu::Vector<3> bias;

    settings.mag_gain_x = gain.x() = ac->mag_gain_x/10000.0f;
    settings.mag_gain_y = gain.y() = ac->mag_gain_y/10000.0f;
    settings.mag_gain_z = gain.z() = ac->mag_gain_z/10000.0f;

    settings.mag_bias_x = bias.x() = ac->mag_bias_x;
    settings.mag_bias_y = bias.y() = ac->mag_bias_y;
    settings.mag_bias_z = bias.z() = ac->mag_bias_z;

    imu::Vector<3> acc;
    settings.accel_bias_x = acc.x() = ac->acc_bias_x/10000.0f;
    settings.accel_bias_y = acc.y() = ac->acc_bias_y/10000.0f;
    settings.accel_bias_z = acc.z() = ac->acc_bias_z/10000.0f;


    ahrs_set_mag_cal(bias, gain);
    ahrs_set_accel_bias(acc);

    write_settings();
    return true;
}

bool Coms::on_get_ahrs_cal(uint8_t* data)
{
    Firelink::m_ahrs_cal ac;
    ac.mag_bias_x = settings.mag_bias_x;
    ac.mag_bias_y = settings.mag_bias_y;
    ac.mag_bias_z = settings.mag_bias_z;

    ac.mag_gain_x = settings.mag_gain_x*10000;
    ac.mag_gain_y = settings.mag_gain_y*10000;
    ac.mag_gain_z = settings.mag_gain_z*10000;

    ac.acc_bias_x = settings.accel_bias_x*10000;
    ac.acc_bias_y = settings.accel_bias_y*10000;
    ac.acc_bias_z = settings.accel_bias_z*10000;

    send_ahrs_cal(&ac);
    return true;
}

bool Coms::on_set_servo_config(uint8_t* data)
{
    Firelink::m_servo_config* sc = (Firelink::m_servo_config*)data;

    settings.servo_signal_scale = sc->scale/100.0;
    settings.elevator_min = sc->ele_min;
    cout << int(sc->ele_min) << endl;
    settings.elevator_max = sc->ele_max;
    settings.aileron_min = sc->ail_min;
    settings.aileron_max = sc->ail_max;
    settings.rudder_min = sc->rud_min;
    settings.rudder_max = sc->rud_max;
    settings.throttle_min = sc->thr_min;
    settings.throttle_max = sc->thr_max;

    write_settings();
    return true;
}

bool Coms::on_get_servo_config(uint8_t* data)
{
    Firelink::m_servo_config sc;
    sc.scale = settings.servo_signal_scale*100;
    sc.ele_min = settings.elevator_min;
    sc.ele_max = settings.elevator_max;
    sc.ail_min = settings.aileron_min;
    sc.ail_max = settings.aileron_max;
    sc.rud_min = settings.rudder_min;
    sc.rud_max = settings.rudder_max;
    sc.thr_min = settings.throttle_min;
    sc.thr_max = settings.throttle_max;

    send_servo_config(&sc);
    return true;
}

bool Coms::on_set_pnr_pid(uint8_t* data)
{
    Firelink::m_pnr_pid* pd = (Firelink::m_pnr_pid*)data;

    settings.pitch_pid.kp = pd->pitch_p/10000.0f;
    settings.pitch_pid.ki = pd->pitch_i/10000.0f;
    settings.pitch_pid.kd = pd->pitch_d/10000.0f;

    settings.roll_pid.kp = pd->roll_p/10000.0f;
    settings.roll_pid.ki = pd->roll_i/10000.0f;
    settings.roll_pid.kd = pd->roll_d/10000.0f;

    write_settings();
    return true;
}

bool Coms::on_get_pnr_pid(uint8_t* data)
{
    Firelink::m_pnr_pid pd;
    pd.pitch_p = settings.pitch_pid.kp*10000.0f;
    pd.pitch_i = settings.pitch_pid.ki*10000.0f;
    pd.pitch_d = settings.pitch_pid.kd*10000.0f;

    pd.roll_p = settings.roll_pid.kp*10000.0f;
    pd.roll_i = settings.roll_pid.ki*10000.0f;
    pd.roll_d = settings.roll_pid.kd*10000.0f;

    send_pnr_pid(&pd);
    return true;
}

bool Coms::on_set_hna_pid(uint8_t* data)
{
    Firelink::m_hna_pid* hp = (Firelink::m_hna_pid*)data;

    heading_controller.Kp = settings.heading_pid.kp = hp->heading_p/100.0f;
    heading_controller.Ki = settings.heading_pid.ki = hp->heading_i/100.0f;
    heading_controller.Kd = settings.heading_pid.kd = hp->heading_d/100.0f;

    alt_controller.Kp = settings.alt_pid.kp = hp->alt_p/100.0f;
    alt_controller.Ki = 0;
    alt_controller.Kd = 0;

    vs_controller.Kp = settings.vs_pid.kp = hp->vs_p/100.0f;
    vs_controller.Ki = settings.vs_pid.ki = hp->vs_i/100.0f;
    vs_controller.Kd = settings.vs_pid.kd = hp->vs_d/100.0f;

    write_settings();
    return true;
}

bool Coms::on_get_hna_pid(uint8_t* data)
{
    Firelink::m_hna_pid hp;

    hp.heading_p = settings.heading_pid.kp*100.0f;
    hp.heading_i = settings.heading_pid.ki*100.0f;
    hp.heading_d = settings.heading_pid.kd*100.0f;

    hp.vs_p = settings.vs_pid.kp*100.0f;
    hp.vs_i = settings.vs_pid.ki*100.0f;
    hp.vs_d = settings.vs_pid.kd*100.0f;

    hp.alt_p = settings.alt_pid.kp*100.0f;

    send_hna_pid(&hp);
    return true;
}

bool Coms::on_set_max_vs(uint8_t* data)
{
    Firelink::m_max_vs* mvs = (Firelink::m_max_vs*)data;

    settings.max_climb_rate = mvs->climb/60.0f; //divide by 60 to convert to feet/second
    settings.max_decent_rate = mvs->descend/60.0f;
    write_settings();
    return true;
}

bool Coms::on_get_max_vs(uint8_t* data)
{
    Firelink::m_max_vs mvs;
    mvs.climb = settings.max_climb_rate*60;
    mvs.descend = settings.max_decent_rate*60;

    send_max_vs(&mvs);
    return true;
}

bool Coms::on_set_path_pid(uint8_t* data)
{
    Firelink::m_path_pid* pp = (Firelink::m_path_pid*)data;
    settings.path_pid.kp = pp->p/1000.0f;
    settings.path_pid.ki = pp->i/1000.0f;
    settings.path_pid.kd = pp->d/1000.0f;

    write_settings();

    return true;
}

bool Coms::on_get_path_pid(uint8_t* data)
{
    Firelink::m_path_pid pp;
    pp.p = settings.path_pid.kp*1000;
    pp.i = settings.path_pid.ki*1000;
    pp.d = settings.path_pid.kd*1000;

    send_path_pid(&pp);
    return true;
}

bool Coms::on_set_at_settings(uint8_t* data)
{
    Firelink::m_at_settings* as = (Firelink::m_at_settings*)data;

    settings.autothrottle.kp = throttle_controller.Kp = as->p/1000.0f;
    settings.autothrottle.ki = throttle_controller.Ki = as->i/1000.0f;
    settings.autothrottle.kd = throttle_controller.Kd = as->d/1000.0f;

    settings.at_settings_pt = as->vs_throttle/10000.0f;
    settings.at_cruise_speed = as->cruise_speed/100.0f;
    settings.at_cruise_power = as->cruise_power;
    settings.min_thr_setting = as->min_throttle;

    write_settings();
    return true;
}

bool Coms::on_get_at_settings(uint8_t* data)
{
    Firelink::m_at_settings as;
    as.p = settings.autothrottle.kp*1000;
    as.i = settings.autothrottle.ki*1000;
    as.d = settings.autothrottle.kd*1000;

    as.vs_throttle = settings.at_settings_pt*10000;
    as.cruise_speed = settings.at_cruise_power*100;
    as.cruise_power = settings.at_cruise_power;
    as.min_throttle = settings.min_thr_setting;

    send_at_settings(&as);
    return true;
}

bool Coms::on_set_mixer_config(uint8_t* data)
{
    Firelink::m_mixer_config* mc = (Firelink::m_mixer_config*)data;

    settings.mixer_config = mc->config;
    settings.mixer_ratio = mc->mix_ratio;
    write_settings();
    return true;
}

bool Coms::on_get_mixer_config(uint8_t* data)
{
    Firelink::m_mixer_config mc;

    mc.config = settings.mixer_config;
    mc.mix_ratio = settings.mixer_ratio;

    send_mixer_config(&mc);
    return true;
}

bool Coms::on_set_loiter_settings(uint8_t* data)
{
    Firelink::m_loiter_settings* ls = (Firelink::m_loiter_settings*)data;

    settings.loiter_radius = ls->radius;
    settings.loiter_correction = ls->p/1000.0f;

    write_settings();
    return true;
}

bool Coms::on_get_loiter_settings(uint8_t* data)
{
    Firelink::m_loiter_settings ls;
    ls.radius = settings.loiter_radius;
    ls.p = settings.loiter_correction*1000;

    send_loiter_settings(&ls);
    return true;
}

bool Coms::on_set_turn_coord(uint8_t* data)
{
    Firelink::m_turn_coord* tc = (Firelink::m_turn_coord*)data;

    settings.yaw_coupling = tc->yaw_coupling/100.0f;
    settings.pitch_coupling = tc->pitch_to_bank/10000.0f;

    write_settings();
    return true;
}

bool Coms::on_get_turn_coord(uint8_t* data)
{
    Firelink::m_turn_coord tc;

    tc.yaw_coupling = settings.yaw_coupling*100;
    tc.pitch_to_bank = settings.pitch_coupling*10000;

    send_turn_coord(&tc);
    return true;
}

bool Coms::on_set_max_angles(uint8_t* data)
{
    Firelink::m_max_angles* ma = (Firelink::m_max_angles*)data;

    settings.max_pitch_up = ma->pitch_up;
    settings.max_pitch_down = ma->pitch_down;
    settings.max_bank_angle = ma->roll;

    settings.max_pitch_rate = ma->pitch_rate/ITERATION_RATE;
    settings.max_roll_rate = ma->roll_rate/ITERATION_RATE;

    write_settings();
    return true;
}

bool Coms::on_get_max_angles(uint8_t* data)
{
    Firelink::m_max_angles ma;
    ma.pitch_up = settings.max_pitch_up;
    ma.pitch_down = settings.max_pitch_down;
    ma.roll = settings.max_bank_angle;

    ma.pitch_rate = settings.max_pitch_rate*ITERATION_RATE;
    ma.roll_rate = settings.max_roll_rate*ITERATION_RATE;

    send_max_angles(&ma);
    return true;
}

bool Coms::on_set_test_mode(uint8_t* data)
{
    Firelink::m_test_mode* tm = (Firelink::m_test_mode*)data;

    switch(tm->type)
    {
    case Firelink::TEST_FUNCTION_HIL_MODE:
    {
        hil_mode_on = true;
    }
    break;
    case Firelink::TEST_FUNCTION_GPS_FAIL:
    {
        //TODO
    }
    break;
    }

    return true;
}

bool Coms::on_set_engage_system(uint8_t* data)
{
    Firelink::m_engage_system* es = (Firelink::m_engage_system*)data;

    switch(es->system)
    {
    case Firelink::SYSTEM_SWITCH_MOTOR:
    {
        motor_on = es->on;
    }
    break;
    case Firelink::SYSTEM_SWITCH_AUTOTHROTTLE:
    {
        autothrottle_on = es->on;
    }
    break;
    case Firelink::SYSTEM_SWITCH_AUTOLAUNCH:
    {
        autolaunch_on = es->on;
    }
    break;
    }
    return true;
}


bool Coms::on_set_battery_mah(uint8_t* data)
{
    Firelink::m_battery_mah* bm = (Firelink::m_battery_mah*)data;
    init_mah = bm->mah;
    return true;
}

bool Coms::on_get_battery_mah(uint8_t* data)
{
    Firelink::m_battery_mah bm;
    bm.mah = init_mah;
    send_battery_mah(&bm);
    return true;
}

bool Coms::on_set_ap_mode(uint8_t* data)
{
    Firelink::m_ap_mode* apm = (Firelink::m_ap_mode*)data;
    if(apm->mode == Firelink::AP_MODE_LOITER)
    {
        loiter_pos = make_coord(inst.latitude, inst.longitude);
        selected_alt = inst.baro_alt;
    }
    if(apm->mode == Firelink::AP_MODE_HNA)
    {
        ap_status.heading = inst.heading;
        selected_alt = inst.baro_alt;
    }
    if(apm->mode == Firelink::AP_MODE_RTB)
    {
        rtb_mode = false; //false for path following
        from_point.pos = make_coord(inst.latitude, inst.longitude);
        selected_alt = inst.baro_alt + 300;
    }
    if(apm->mode == Firelink::AP_MODE_WAYPOINTS)
    {
        from_point.pos = make_coord(inst.latitude, inst.longitude);
        current_waypoint = 0;
        wp_hit = false;

        if(distance_between(from_point.pos, points[0].pos) > 500)
        {
            loiter_pos = make_coord(inst.latitude, inst.longitude);
            selected_alt = inst.baro_alt;
        }
    }

    if(apm->mode >= Firelink::AP_MODE_HNA)
        vs_controller.integral = 0;

    ap_status.mode = apm->mode;
    return true;
}

bool Coms::on_set_alarm_actions(uint8_t* data)
{
    Firelink::m_alarm_actions* aa = (Firelink::m_alarm_actions*)data;

    settings.low_bat_action = aa->low_bat;
    settings.boundary_action = aa->boundary;
    settings.no_gps_action = aa->no_gps;
    settings.lost_control_action = aa->lost_control;
    settings.no_telem_action = aa->no_telem;

    write_settings();
    return true;
}

bool Coms::on_get_alarm_actions(uint8_t* data)
{
    Firelink::m_alarm_actions aa;

    aa.boundary = settings.boundary_action;
    aa.low_bat = settings.low_bat_action;
    aa.no_gps = settings.no_gps_action;
    aa.lost_control = settings.lost_control_action;
    aa.no_telem = settings.no_telem_action;

    send_alarm_actions(&aa);
    return true;
}

bool Coms::on_set_vh_stream_prio(uint8_t* data)
{
    Firelink::m_vh_stream_prio* sp = (Firelink::m_vh_stream_prio*)data;
    set_stream_prio(sp->type, sp->prio);
    return true;
}

bool Coms::on_get_vh_stream_prio(uint8_t* data)
{
    Firelink::m_vh_stream_prio sp = *(Firelink::m_vh_stream_prio*)data;
    sp.prio = get_stream_prio(sp.type);
    send_vh_stream_prio(&sp);
    return true;
}


bool Coms::on_set_keep_alive(uint8_t* data)
{
    Firelink::m_keep_alive* ka = (Firelink::m_keep_alive*)data;
    keep_alive_seconds = ka->s;
    return true;
}

bool Coms::on_set_flight_command(uint8_t* data)
{
    Firelink::m_flight_command* fc = (Firelink::m_flight_command*)data;
    switch(fc->cmd)
    {
    case Firelink::FLIGHT_COMMAND_NEXT_WAYPOINT:
    {
        current_waypoint++;
        from_point.pos = make_coord(inst.latitude, inst.longitude);
        from_point.alt = inst.baro_alt;
    }
    break;
    case Firelink::FLIGHT_COMMAND_BACK_WAYPOINT:
    {
        if(current_waypoint != 0)
        {
            current_waypoint--;
            from_point.pos = make_coord(inst.latitude, inst.longitude);
            from_point.alt = inst.baro_alt;
        }
    }
    break;
    case Firelink::FLIGHT_COMMAND_RESTART:
    {
        current_waypoint = 0;
        from_point.pos = make_coord(inst.latitude, inst.longitude);
        from_point.alt = inst.baro_alt;
    }
    break;
    }
    return true;
}


bool Coms::on_set_mode_select_settings(uint8_t* data)
{
    Firelink::m_mode_select_settings* ms = (Firelink::m_mode_select_settings*)data;

    settings.mode_select_mode = ms->mode;

    switch(ms->chan)
    {
    case 1:
    {
        settings.mode_select_channel = SERVO_5_CHANNEL;
    }
    break;
    case 2:
    {
        settings.mode_select_channel = SERVO_6_CHANNEL;
    }
    break;
    default:
    {
        settings.mode_select_channel = 0;
    }
    }

    write_settings();
    return true;
}

bool Coms::on_get_mode_select_settings(uint8_t* data)
{
    Firelink::m_mode_select_settings ms;

    ms.chan = 0;
    if(settings.mode_select_channel == SERVO_5_CHANNEL)
        ms.chan = 1;
    if(settings.mode_select_channel == SERVO_6_CHANNEL)
        ms.chan = 2;

    ms.mode = settings.mode_select_mode;

    send_mode_select_settings(&ms);
    return true;
}

bool Coms::on_set_channel_inversions(uint8_t* data)
{
    Firelink::m_channel_inversions* ci = (Firelink::m_channel_inversions*)data;

    settings.input_channel_invert = ci->in;
    settings.autopilot_channel_invert = ci->ap;
    settings.output_channel_invert = ci->out;
    write_settings();
    return true;
}

bool Coms::on_get_channel_inversions(uint8_t* data)
{
    Firelink::m_channel_inversions ci;
    ci.in = settings.input_channel_invert;
    ci.ap = settings.autopilot_channel_invert;
    ci.out = settings.output_channel_invert;

    send_channel_inversions(&ci);
    return true;
}

bool Coms::on_set_skip_queue(uint8_t* data)
{
    Firelink::m_skip_queue* sq = (Firelink::m_skip_queue*)data;
    if(sq->skip)
    {
        set_dont_skip_queue(false);
    }
    else
    {
        set_dont_skip_queue(true);
    }
    return true;
}
