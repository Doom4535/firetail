#include "settings.h"


_config settings;

int write_settings()
{
    const uint8_t* p = (const uint8_t*)&settings;
    unsigned int i;
    for (i = 0; i < sizeof(settings); i++)
        EEPROM.write(i, *p++);
    return i;
}

int read_settings()
{
    uint8_t* p = (uint8_t*)(void*)&settings;
    unsigned int i;
    for (i = 0; i < sizeof(settings); i++)
        *p++ = EEPROM.read(i);
    return i;
}

