/*
    FireTail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef UIMU_AHRS_H
#define UIMU_AHRS_H

#include <Arduino.h>
#include <ChibiOS_ARM.h>
#include "i2c_t3.h"
#include "config.h"
#include "imumaths/imumaths.h"
#include "airdata.h"

#include "pca_servo.h"



//for the purpose of troubleshooting
//#define AHRS_GYRO_ONLY
//#define AHRS_ACCEL_ONLY
#define AHRS_NORMAL_COMPLEMENT


//initialises the AHRS
void ahrs_init();

void ahrs_set_offset(imu::Quaternion o);

//sets the beta. this controls how strong the drift correction will be.
//a higher beta means more correction
void ahrs_set_beta(float beta);

void ahrs_set_velocity(float velocity);


//does an iteration. call this every 20ms at least
void ahrs_iterate();

//returns the orientation in various forms
imu::Vector<3> ahrs_get_euler(); //heading, pitch, roll in degrees
imu::Matrix<3> ahrs_get_matrix(); //north-east-down rotation matrix
imu::Quaternion ahrs_get_quaternion(); //good ol' quaternion

imu::Quaternion ahrs_get_imu_quaternion();


void ahrs_set_mag_cal(imu::Vector<3> bias, imu::Vector<3> scale);
void ahrs_get_mag_cal(imu::Vector<3>* bias, imu::Vector<3>* scale);

void ahrs_set_accel_bias(imu::Vector<3> ab);
imu::Vector<3> ahrs_get_accel_bias();

imu::Vector<3> ahrs_get_mag();

imu::Vector<3> ahrs_raw_ang_vel();
imu::Vector<3> ahrs_world_acceleration();

extern PWMServoDriver servo_driver;

#endif

