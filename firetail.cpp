/*
    Firetail UAV System

	firetail.cpp

	This file contains the initialisation function and the
	main loop.

    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "firetail.h"
#include "terminal.h"
#include "coms.h"
#include "control.h"
#include "utility.h"

Firelink::vs_inst inst;
Firelink::vs_systems systems;
Firelink::vs_ap_status ap_status;
Firelink::vs_raw_marg raw_marg;
Firelink::vs_raw_pwm raw_pwm;

Coms coms;
Coms coms1;

ArduinoOutStream cout(FG_PORT);


Servo servos[6]; //the servos

bool gps_initialised; //false to start with, is set to 'true' when GPS gets a 3D lock for the first time

int delta_t;
float dt;

waypoint home;

uint32_t keep_alive_seconds;
uint32_t last_pack_millis;

//these rates define how quickly the controller moves the selected heading & altitude bugs
float heading_control_rate;
float altitude_control_rate;

float mag_var; //magnetic variation

coord loiter_pos;

float selected_alt; //need to keep a 'high resolution' floating point value for selected alt
// using the short int vd.ap_alt chops of decimal places, which causes issues when trying to slowly change selected altitude

//these variables are for pitch and roll rate limiting
int16_t selected_roll;
int16_t selected_pitch;


waypoint points[512];
int current_waypoint;
waypoint from_point;
float xtrack_integral;
float last_xtrack_dist;
ExpoMovingAvg xtrack_der;
bool wp_hit;

bool rtb_mode; //true for loitering, false for path following

waypoint geopoints[512];

//milliamp-hours consumed
float mahs_used;
//initial battery capacity
uint32_t init_mah;

//when this hits 100, then the geofence boundary has been crossed.
int boundary_alarm_counter;

//alarms for various abnormal situations
bool boundary_alarm;
bool low_bat_alarm;
bool no_gps_alarm;
bool no_datalink_alarm;
bool no_signal_alarm;

Firelink::ALARM_ACTION last_action;

CurrentSensor current_sensor(CURRENT_PIN);
ExpoMovingAvg voltage_filter;

ExpoMovingAvg gps_acceleration;
float last_speed;


Firelink::AP_MODE mode_select_previous_mode;
bool mode_selected;

bool motor_on;
bool autothrottle_on;
bool autolaunch_on;
bool has_rc;

static uint32_t home_position_sent_millis;

/**
Initialisation function.
*/
void firetail_begin()
{
    keep_alive_seconds = 10;
    //make sure HIL mode is off
    motor_on = false;
    autothrottle_on = false;
    autolaunch_on = false;

    hil_mode_on = false;

    no_signal_alarm = false;
    boundary_alarm = false;
    mode_selected = false;
    mahs_used = 0.0;

    FG_PORT.begin(19200); //FlightGear/simulator port
    GPS_PORT.begin(9600); //GPS port. 9600 is the default baud rate for adafruit ultimate GPS
    //but it's be reset to 38400
    FL1_PORT.begin(57600); //FireLink (UAV Coms)
    FL2_PORT.begin(57600); //FireLink2

    FL1_PORT.setTimeout(0);
    FL2_PORT.setTimeout(0);
    GPS_PORT.setTimeout(0);
    FG_PORT.setTimeout(0);

    chThdSleepMilliseconds(1000); //pause for a moment while the GPS starts up

    //switch the GPS baud rate to 38400

    //we'll do a PMTK command for MTK3339 chips
    GPS_PORT.write("$PMTK251,38400*27\r\n");
    GPS_PORT.flush();

    //and a PUBX command for ublox devices
    GPS_PORT.write("$PUBX,41,1,0007,0003,38400,0*20\r\n");
    GPS_PORT.flush();

    GPS_PORT.end();
    chThdSleepMilliseconds(100);
    GPS_PORT.begin(38400);


    //for MTK3339 chips
    //this gives VTG, GGA, GSA NMEA messages only
    //tell the GPS that we want all information
    GPS_PORT.write("$PMTK314,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n");
    //set GPS update rate to 1Hz
    GPS_PORT.write("$PMTK220,1000*1F\r\n");

    //for ublox chips
    GPS_PORT.write("$PUBX,40,GLL,0,0,0,0,0,0*5C\r\n"); //don't care about GGL
    GPS_PORT.write("$PUBX,40,GSV,5,5,5,5,5,5*59\r\n"); //give us a GSV every one in five epochs
    GPS_PORT.write("$PUBX,40,RMC,5,5,5,5,5,5*47\r\n"); //give us an RMC every one in five epochs

    //finally tell the thing to switch to 5Hz update rate
    const uint8_t ublox_rate[] = {0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0xC8, 0x00, 0x01, 0x00, 0x01, 0x00, 0xDE, 0x6A};
    GPS_PORT.write(ublox_rate, sizeof(ublox_rate));

    gps_initialised = false;

    I2C_BUS.begin(I2C_MASTER, 0, I2C_PINS_18_19, I2C_PULLUP_EXT, I2C_RATE_100);
    mag_var = 0;
    ad_init(); //initialise air data
    ahrs_init(); //initialise AHRS

    servo_driver.begin();
    servo_driver.setPWMFreq(60);

    read_settings();

    imu::Vector<3> ab;
    ab.x() = settings.accel_bias_x;
    ab.y() = settings.accel_bias_y;
    ab.z() = settings.accel_bias_z;
    ahrs_set_accel_bias(ab);

    ab.x() = settings.mag_bias_x;
    ab.y() = settings.mag_bias_y;
    ab.z() = settings.mag_bias_z;

    imu::Vector<3> msc;
    msc.x() = settings.mag_gain_x;
    msc.y() = settings.mag_gain_y;
    msc.z() = settings.mag_gain_z;
    ahrs_set_mag_cal(ab, msc);

    sd_card_init();
    sd_card_load_plan(points, geopoints);
    sd_card_start_logging();

    rc_control_setup();

    servos[ELEVATOR_CHANNEL].attach(ELEVATOR_SERVO_OUT);
    servos[AILERON_CHANNEL].attach(AILERON_SERVO_OUT);
    servos[THROTTLE_CHANNEL].attach(THROTTLE_SERVO_OUT);
    servos[RUDDER_CHANNEL].attach(RUDDER_SERVO_OUT);
    servos[SERVO_5_CHANNEL].attach(SERVO_5_OUT);
    servos[SERVO_6_CHANNEL].attach(SERVO_6_OUT);

    delta_t = ms();

    pitch_controller.Kp = settings.pitch_pid.kp;
    pitch_controller.Ki = settings.pitch_pid.ki;
    pitch_controller.Kd = settings.pitch_pid.kd;

    pitch_controller.integral_constraint = 500;
    pitch_controller.windup_limit = 2000000;

    roll_controller.Kp = settings.roll_pid.kp;
    roll_controller.Ki = settings.roll_pid.ki;
    roll_controller.Kd = settings.roll_pid.kd;

    roll_controller.integral_constraint = 104;
    roll_controller.windup_limit = 15000000;

    heading_controller.Kp = settings.heading_pid.kp;
    heading_controller.Ki = settings.heading_pid.ki;
    heading_controller.Kd = settings.heading_pid.kd;

    heading_controller.integral_constraint = 20;
    heading_controller.windup_limit = 15000;

    vs_controller.Kp = settings.vs_pid.kp;
    vs_controller.Ki = settings.vs_pid.ki;
    vs_controller.Kd = settings.vs_pid.kd;

    vs_controller.integral_constraint = 2500;
    vs_controller.windup_limit = 20000000;

    alt_controller.Kp = settings.alt_pid.kp;
    alt_controller.Ki = 0;
    alt_controller.Kd = 0;

    throttle_controller.Kp = settings.autothrottle.kp;
    throttle_controller.Ki = settings.autothrottle.ki;
    throttle_controller.Kd = settings.autothrottle.kd;

    throttle_controller.windup_limit = 1000;
    throttle_controller.integral_constraint = 10000;

    FuzzyMember* fs = speed_set.get_set(0);
    fs->set_minmax(-20, -4);
    fs->set_value(127);

    fs = speed_set.get_set(1);
    fs->set_minmax(-8, 0);
    fs->set_value(20);

    fs = speed_set.get_set(2);
    fs->set_minmax(-10, 10);
    fs->set_value(0);

    fs = speed_set.get_set(3);
    fs->set_minmax(0, 8);
    fs->set_value(-25);

    fs = speed_set.get_set(4);
    fs->set_minmax(4, 20);
    fs->set_value(-127);

    heading_control_rate = 1.5;
    altitude_control_rate = 0.2;

    voltage_filter = ExpoMovingAvg(0.05);

    xtrack_der = ExpoMovingAvg(0.1);

    selected_roll = 0;

    current_sensor.set_bias(123);
    current_sensor.set_sensitivity(60);
}


/**
The main loop.
*/
void firetail_iterate()
{
    //read from GPS, don't need to limit the cycle time for this
    while(GPS_PORT.available())
        gps.encode(GPS_PORT.read());

    term_iterate();
    if(hil_mode_on)
    {
        while(FG_PORT.available())
            hil_encode(FG_PORT.read());
    }

    //keep iteration time at the chosen rate
    if((ms() - delta_t) < ITERATION_DELAY_MS)
        return;

    dt = ms() - delta_t;
    dt /= 1000.0;
    delta_t = ms();

    //disconnect datalink if emergency override is on
    if(ap_status.mode != Firelink::AP_MODE_EMERGENCY_OVERRIDE)
    {
        while(FL1_PORT.available())
        {
            uint8_t c = FL1_PORT.read();
            if(coms.encode(c) != -1)
            {
                last_pack_millis = ms();
            }
        }

        while(FL2_PORT.available())
            coms1.encode(FL2_PORT.read());

        systems.timer = ms()/1000.0;

        systems.switches = set_bit(systems.switches, Firelink::SYSTEM_SWITCH_MOTOR+1, motor_on);
        systems.switches = set_bit(systems.switches, Firelink::SYSTEM_SWITCH_AUTOTHROTTLE+1, autothrottle_on);
        systems.switches = set_bit(systems.switches, Firelink::SYSTEM_SWITCH_NO_SIGNAL+1, no_signal_alarm);
        systems.switches = set_bit(systems.switches, Firelink::SYSTEM_SWITCH_HIT_BOUNDARY+1, boundary_alarm);
        systems.switches = set_bit(systems.switches, Firelink::SYSTEM_SWITCH_AUTOLAUNCH+1, autolaunch_on);

        unsigned char pack[40];
        //if the last packet was received within the keep-alive time, then send a reply
        if((ms() - last_pack_millis) < (keep_alive_seconds*1000))
        {
            coms.next_pack(pack);
            FL1_PORT.write(pack, 38);
        }

        coms1.next_pack(pack);
        FL2_PORT.write(pack, 38);

        if((ms()-last_pack_millis) > 20000)
        {
            /*
            coms.push_alarm_activated(Firelink::ALARM_TYPE_LOST_DATALINK); //mostly pointless because there is no datalink now
            coms1.push_alarm_activated(Firelink::ALARM_TYPE_LOST_DATALINK);
            */
            no_datalink_alarm = true;
        }
        else
            no_datalink_alarm = false;
    }

    if(hil_mode_on)
        ahrs_set_velocity(hil_speed);



    systems.milliamps = current_sensor.readAmps()*1000;
    voltage_filter.step(analogRead(VOLTAGE_PIN));
    //this assumes a 10k/1.5k voltage divider
    //this gives a 115/15 ratio, or 7.6666666
    //to convert raw analog reading to voltage, multiply by 0.003222656
    //0.024707029 = 7.6666666 * 0.003222656
    //and then to get tenths of a volt, multiply by 100
    systems.volts = voltage_filter.get() * 2.4707029;

    //time to calculate milliamp-hours
    mahs_used += systems.milliamps / (180000.0);

    if(init_mah > 0)
        systems.power_remaining = 100 - constrain(mahs_used/init_mah*100, 0, 100);
    else
        systems.power_remaining = 0;

    if((systems.power_remaining > 0) and (systems.power_remaining < 10) and (not low_bat_alarm))
    {
        if(low_bat_alarm == false)
        {
            /*
            coms.push_alarm_activated(Firelink::ALARM_TYPE_LOW_BAT);
            coms1.push_alarm_activated(Firelink::ALARM_TYPE_LOW_BAT);
            */
        }
        low_bat_alarm = true;
        if(settings.low_bat_action == Firelink::ALARM_ACTION_RTB)
        {
            from_point.pos = make_coord(inst.latitude, inst.longitude);
            from_point.alt = inst.baro_alt;
        }
    }

    //get euler angles from the AHRS
    imu::Vector<3> euler = ahrs_get_euler();

    inst.baro_alt = ad_pressure_alt();
    inst.vert_speed = ad_vert_speed();

    inst.heading = (euler.x()*100) + (mag_var*100);
    inst.pitch = euler.y()*100;
    inst.roll = euler.z()*100;
    systems.temperature = ad_temp()*100;
    ap_status.alt = selected_alt; //need to give firelink a copy of selected altitude

    pos_iterate();
    coord position = pos_get();
    inst.latitude = position.lat*1000000;
    inst.longitude = position.lng*1000000;

    systems.ground_speed = pos_get_speed()*100;

    int gt = pos_get_track()*100;
    while(gt > 18000)
        gt -= 36000;
    while(gt < -18000)
        gt += 36000;

    systems.ground_track = gt;
    if(!hil_mode_on)
    {
        systems.sats_used = gps.sats_in_use;
        systems.gps_lock = Firelink::GPS_LOCK(gps.fix);

        if((ms() - gps.fix_age) > 5000) //if there hasn't been a GPS message containing fix data for over five seconds
        {
            systems.gps_lock = Firelink::GPS_LOCK_NONE; //assume the worst case
        }

        if(gps.fix == CompleteGPS::FIX_3D)
        {
            if((ms() - gps.speed_age) < ITERATION_DELAY_MS)
            {
                float spd = gps.speed/100.0;
                ahrs_set_velocity(spd);

                spd *= KMH_TO_MS; //convert to m/s to derive acceleration
                gps_acceleration.step((last_speed - spd)/dt);
                last_speed = spd;
            }
        }
        if((!gps_initialised)
                && (systems.gps_lock == Firelink::GPS_LOCK_3D)
                && (ad_vert_speed() < 5.0)
                && (ms() > 15000)) //if we've got a lock for the first time, air data is stable and sensor loop has started
        {
            gps_initialised = true;
            home.pos.lat = gps.latitude; //save the current position and call it 'home'
            home.pos.lng = gps.longitude;
            home.alt = ad_pressure_alt();

            Firelink::m_home_coordinates hm;
            hm.alt = home.alt;
            hm.lat = home.pos.lat*1000000;
            hm.lng = home.pos.lng*1000000;

            coms.send_home_coordinates(&hm);
            coms1.send_home_coordinates(&hm);

            home_position_sent_millis = ms();

            //tell the GPS that we only care about nav data now
            //this gives VTG, GGA, GSA NMEA messages only
            GPS_PORT.write("$PMTK314,0,0,1,1,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0*2D\r\n");

            chThdSleepMilliseconds(10);
            //change GPS update rate to 10Hz
            GPS_PORT.write("$PMTK220,100*2F\r\n");
        }
        else
        {
            if((ms()-home_position_sent_millis) > 15000) //send home position every 15 seconds
            {
                Firelink::m_home_coordinates hm;
                hm.alt = home.alt;
                hm.lat = home.pos.lat*1000000;
                hm.lng = home.pos.lng*1000000;

                coms.send_home_coordinates(&hm);
                coms1.send_home_coordinates(&hm);
                home_position_sent_millis = ms();
            }
        }

        if((gps_initialised)
                && (systems.gps_lock < Firelink::GPS_LOCK_2D))
        {
            if(no_gps_alarm == false)
            {
                /*
                coms.push_alarm_activated(Firelink::ALARM_TYPE_GPS_NOLOCK);
                coms1.push_alarm_activated(Firelink::ALARM_TYPE_GPS_NOLOCK);
                */
            }
            no_gps_alarm = true;
        }
        else
            no_gps_alarm = false;
    }
    else
    {
        gps_acceleration.step((last_speed - hil_speed)/dt);
        systems.gps_lock = Firelink::GPS_LOCK_3D;
    }


    while(inst.heading > 18000)
        inst.heading -= 36000;
    while(inst.heading < -18000)
        inst.heading += 36000;

    while(systems.ground_track > 18000)
        systems.ground_track -= 36000;
    while(systems.ground_track < -18000)
        systems.ground_track += 36000;

    inst.air_speed = ad_airspeed()*100;

    //OK, sensors and communications stuff has been done.
    //get control inputs
    int16_t control_in[4];
    int16_t control_out[6];

    if(!rc_have_signal())
    {
        //don't activate no signal alarm repeatedly
        if(no_signal_alarm == false)
        {
            /*
            coms.push_alarm_activated(Firelink::ALARM_TYPE_LOST_CONTROL);
            coms1.push_alarm_activated(Firelink::ALARM_TYPE_LOST_CONTROL);
            */
        }

        no_signal_alarm = true;
    }
    else
        no_signal_alarm = false;

    //incase you just want to fly your plane in manual, without telemetery and laptop
    //you can arm the motor by setting the throttle to off, elevator all the way up
    //and rudder & ailerons to maximum deflection
    //this only arms the motor when there is no datalink
    if((rc_get_channel(ELEVATOR_CHANNEL) >= 1840)
            and (rc_get_channel(THROTTLE_CHANNEL) <= 1160)
            and (rc_get_channel(RUDDER_CHANNEL) >= 1840)
            and (rc_get_channel(AILERON_CHANNEL) >= 1840)
            and no_datalink_alarm)
    {
        motor_on = true;
    }

    control_in[ELEVATOR_CHANNEL] = constrain(((rc_get_channel(ELEVATOR_CHANNEL)-1500)*0.256*settings.servo_signal_scale), -127, 127);
    control_in[AILERON_CHANNEL] = constrain(((rc_get_channel(AILERON_CHANNEL)-1500)*0.256*settings.servo_signal_scale), -127, 127);
    control_in[RUDDER_CHANNEL] = constrain(((rc_get_channel(RUDDER_CHANNEL)-1500)*0.256*settings.servo_signal_scale), -127, 127);
    control_in[THROTTLE_CHANNEL] = constrain(((rc_get_channel(THROTTLE_CHANNEL)-1500)*0.256*settings.servo_signal_scale), -127, 127);

    //set pwm inputs for coms
    for(int i = 0; i < 6; i++)
        raw_pwm.channels[i] = rc_get_channel(i);


    if(read_bit(settings.input_channel_invert, ELEVATOR_CHANNEL+1))
        control_in[ELEVATOR_CHANNEL] *= -1;
    if(read_bit(settings.input_channel_invert, AILERON_CHANNEL+1))
        control_in[AILERON_CHANNEL] *= -1;
    if(read_bit(settings.input_channel_invert, RUDDER_CHANNEL+1))
        control_in[RUDDER_CHANNEL] *= -1;
    if(read_bit(settings.input_channel_invert, THROTTLE_CHANNEL+1))
        control_in[THROTTLE_CHANNEL] *= -1;

//mode select stuff
//only use mode select if the mode select channel is either 5 or 6
    if((settings.mode_select_channel == SERVO_5_CHANNEL) || (settings.mode_select_channel == SERVO_6_CHANNEL))
    {
        if((ms()-last_pack_millis) > 2000) //only works if there is NO DATALINK. if you've got a laptop setup why not use it?
        {
            if((rc_get_channel(settings.mode_select_channel) > 1600) && (!mode_selected))
            {
                mode_select_previous_mode = ap_status.mode;
                ap_status.mode = settings.mode_select_mode;
                mode_selected = true;
            }
        }
        if((mode_selected) && (rc_get_channel(settings.mode_select_channel) < 1400))
        {
            ap_status.mode = mode_select_previous_mode;
            mode_selected = false;
        }
    }

//the boundary alarm goes off when the vehicle has hit the geofence
//it's only sensible to do this when the GPS has a fix AND there are at least 3 geofence coordinates uploaded

    bool more_than_three_geopoints = true;
    for(int j = 0; j < 3; j++)
    {
        if(!geopoints[j].used)
        {
            more_than_three_geopoints = false;
            break;
        }
    }
    if(((systems.gps_lock == Firelink::GPS_LOCK_2D)
            || (systems.gps_lock == Firelink::GPS_LOCK_3D))

            && (more_than_three_geopoints))
    {
        //use a ray casting algorithm to determine if the aircraft is inside the fence
        //this algorithm DOES NOT use great-circle maths.
        //It isn't suitable for very long distance journeys (100km or more), especially in polar regions.
        //if you are flying around the south pole, don't use geofencing.
        coord here = make_coord(inst.latitude, inst.longitude);
        bool oddNodes = false;
        int i;
        for(i = 0; i < 512; i++)
        {
            if(geopoints[i+1].used == false)
                break;
            if(((geopoints[i].pos.lng > here.lng) != (geopoints[i+1].pos.lng > here.lng)) &&
                    (here.lat < (geopoints[i+1].pos.lat - geopoints[i].pos.lat) * (here.lng - geopoints[i].pos.lng) / (geopoints[i+1].pos.lng - geopoints[i].pos.lng) + geopoints[i].pos.lat))
            {
                oddNodes = !oddNodes;
            }
        }

        if(((geopoints[i].pos.lng > here.lng) != (geopoints[0].pos.lng > here.lng)) &&
                (here.lat < (geopoints[0].pos.lat - geopoints[i].pos.lat) * (here.lng - geopoints[i].pos.lng) / (geopoints[0].pos.lng - geopoints[i].pos.lng) + geopoints[i].pos.lat))
        {
            oddNodes = !oddNodes;
        }

        if(!oddNodes)
        {
            //it is outside the fence
            boundary_alarm_counter = constrain((boundary_alarm_counter+1), 0, 100);
        }
        else
        {
            //inside the fence and OK
            boundary_alarm_counter = constrain((boundary_alarm_counter-1), 0, 100);
        }
    }
    else
    {
        boundary_alarm_counter = constrain((boundary_alarm_counter-1), 0, 100);
    }

    if(boundary_alarm_counter > 99)
    {
        if(boundary_alarm == false)
        {
            /*
            coms.push_alarm_activated(Firelink::ALARM_TYPE_BOUNDARY);
            coms1.push_alarm_activated(Firelink::ALARM_TYPE_BOUNDARY);
            */
        }

        boundary_alarm = true;
    }
    if(boundary_alarm_counter < 5)
        boundary_alarm = false;

//now it's time to workout how to move the servos

//mode select stuff
    if(ap_status.mode == Firelink::AP_MODE_EMERGENCY_OVERRIDE)
    {
        control_out[ELEVATOR_CHANNEL] = control_in[ELEVATOR_CHANNEL];
        control_out[AILERON_CHANNEL] = control_in[AILERON_CHANNEL];
        control_out[RUDDER_CHANNEL] = control_in[RUDDER_CHANNEL];
        control_out[THROTTLE_CHANNEL] = control_in[THROTTLE_CHANNEL];
    }
    else
        control_iteration(control_in, control_out, dt);

//if hil mode is off, output FMS Pic protocol
    if(!hil_mode_on)
    {
        FG_PORT.write(255);
        for(int i = 0; i < 4; i++)
        {
            int r = constrain(control_out[i] + 127, 0, 254);
            FG_PORT.write(r);
        }

        //channel mixing here for V tail and flying wing configurations
        switch(settings.mixer_config)
        {
        case Firelink::MIXER_CONFIGURATION_NONE:
        {
            //no mixing, so do nothing
        }
        break;
        case Firelink::MIXER_CONFIGURATION_V_TAIL:
        {
            int ele_mix = control_out[ELEVATOR_CHANNEL]*settings.mixer_ratio;
            int rud_mix = control_out[RUDDER_CHANNEL]*(100-settings.mixer_ratio);

            control_out[ELEVATOR_CHANNEL] = (rud_mix + ele_mix)/50.0;
            control_out[RUDDER_CHANNEL] = (rud_mix - ele_mix)/50.0;
        }
        break;
        case Firelink::MIXER_CONFIGURATION_FLYING_WING:
        {
            int ele_mix = control_out[ELEVATOR_CHANNEL]*settings.mixer_ratio;
            int ail_mix = control_out[AILERON_CHANNEL]*(100-settings.mixer_ratio);

            control_out[ELEVATOR_CHANNEL] = (ail_mix + ele_mix)/100.0;
            control_out[AILERON_CHANNEL] = (ail_mix - ele_mix)/100.0;
        }
        break;
        }

    //if the motor is disarmed, make sure it stays OFF
        if(!motor_on)
            control_out[THROTTLE_CHANNEL] = -127;

    //constrain signals so the servos don't drive too hard and damage something
        control_out[ELEVATOR_CHANNEL] = constrain(control_out[ELEVATOR_CHANNEL], settings.elevator_min, settings.elevator_max);
        control_out[AILERON_CHANNEL] = constrain(control_out[AILERON_CHANNEL], settings.aileron_min, settings.aileron_max);
        control_out[RUDDER_CHANNEL] = constrain(control_out[RUDDER_CHANNEL], settings.rudder_min, settings.rudder_max);
        control_out[THROTTLE_CHANNEL] = constrain(control_out[THROTTLE_CHANNEL], settings.throttle_min, settings.throttle_max);

    //write PWM signals to servo pins
        if(read_bit(settings.output_channel_invert, ELEVATOR_CHANNEL+1))
            servos[ELEVATOR_CHANNEL].write(-control_out[ELEVATOR_CHANNEL]*CHAR_TO_SERVO_POS+90);
        else
            servos[ELEVATOR_CHANNEL].write(control_out[ELEVATOR_CHANNEL]*CHAR_TO_SERVO_POS+90);

        if(read_bit(settings.output_channel_invert, AILERON_CHANNEL+1))
            servos[AILERON_CHANNEL].write(-control_out[AILERON_CHANNEL]*CHAR_TO_SERVO_POS+90);
        else
            servos[AILERON_CHANNEL].write(control_out[AILERON_CHANNEL]*CHAR_TO_SERVO_POS+90);

        if(read_bit(settings.output_channel_invert, RUDDER_CHANNEL+1))
            servos[RUDDER_CHANNEL].write(-control_out[RUDDER_CHANNEL]*CHAR_TO_SERVO_POS+90);
        else
            servos[RUDDER_CHANNEL].write(control_out[RUDDER_CHANNEL]*CHAR_TO_SERVO_POS+90);

        if(read_bit(settings.output_channel_invert, THROTTLE_CHANNEL+1))
            servos[THROTTLE_CHANNEL].write(-control_out[THROTTLE_CHANNEL]*CHAR_TO_SERVO_POS+90);
        else
            servos[THROTTLE_CHANNEL].write(control_out[THROTTLE_CHANNEL]*CHAR_TO_SERVO_POS+90);




    //shuttle RC Rx to servo out for these two
        servos[SERVO_5_CHANNEL].writeMicroseconds(rc_get_channel(SERVO_5_CHANNEL));
        servos[SERVO_6_CHANNEL].writeMicroseconds(rc_get_channel(SERVO_6_CHANNEL));

        control_out[4] = constrain(((rc_get_channel(SERVO_5_CHANNEL)-1500)*0.256*settings.servo_signal_scale), -127, 127);
        control_out[5] = constrain(((rc_get_channel(SERVO_6_CHANNEL)-1500)*0.256*settings.servo_signal_scale), -127, 127);

        servo_driver.set_outputs(control_out);
    }
    else
    {
        hil_write(control_out);
    }


}



void mainThread(); //main ChibiOS thread

int main()
{
    chBegin(mainThread);
}


void mainThread()
{
    firetail_begin();

    delay(100);

    while(true)
    {
        firetail_iterate();
        chThdYield();
    }
}



