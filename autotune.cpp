/*
    Firetail UAV System

	autotune.cpp

    A relay method PID autotuner.
	Inspired by the work of Brett Beauregard and William Spinelli
	http://brettbeauregard.com/blog/2012/01/arduino-pid-autotune-library/

    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "autotune.h"
#include "hil.h"
#include <math.h>


PIDTuner::PIDTuner()
{
    controlType = 0;
    noiseBand = 0.5;
    running = false;
    oStep = 30;
    SetLookbackSec(10);
    lastTime = ms();
}



void PIDTuner::Cancel()
{
    running = false;
}

int PIDTuner::Runtime()
{
    justevaled=false;
    if(peakCount>9 && running)
    {
        running = false;
        FinishUp();
        return 1;
    }
    unsigned long now = ms();

    if((now-lastTime) < (unsigned long)(sampleTime))
        return false;

    lastTime = now;
    float refVal = *input;
    justevaled = true;

    if(!running)
    {
        //initialize working variables the first time around
        peakType = 0;
        peakCount=0;
        justchanged=false;
        absMax=refVal;
        absMin=refVal;
        setpoint = refVal;
        running = true;
        outputStart = *output;
        *output = outputStart+oStep;
    }
    else
    {
        if(refVal>absMax)absMax=refVal;
        if(refVal<absMin)absMin=refVal;
    }

    //oscillate the output base on the input's relation to the setpoint

    if(refVal>setpoint+noiseBand) *output = outputStart-oStep;
    else if (refVal<setpoint-noiseBand) *output = outputStart+oStep;


    //bool isMax=true, isMin=true;
    isMax=true;
    isMin=true;
    //id peaks
    for(int i=nLookBack-1; i>=0; i--)
    {
        float val = lastInputs[i];
        if(isMax) isMax = refVal>val;
        if(isMin) isMin = refVal<val;
        lastInputs[i+1] = lastInputs[i];
    }
    lastInputs[0] = refVal;
    if(nLookBack<9)
    {
        //we don't want to trust the maxes or mins until the inputs array has been filled
        return 0;
    }

    if(isMax)
    {
        if(peakType==0)peakType=1;
        if(peakType==-1)
        {
            peakType = 1;
            justchanged=true;
            peak2 = peak1;
        }
        peak1 = now;
        peaks[peakCount] = refVal;

    }
    else if(isMin)
    {
        if(peakType==0)peakType=-1;
        if(peakType==1)
        {
            peakType=-1;
            peakCount++;
            justchanged=true;
        }

        if(peakCount<10)peaks[peakCount] = refVal;
    }

    if(justchanged && peakCount>2)
    {
        //we've transitioned.  check if we can autotune based on the last peaks
        float avgSeparation = (abs(peaks[peakCount-1]-peaks[peakCount-2])+abs(peaks[peakCount-2]-peaks[peakCount-3]))/2;
        if( avgSeparation < 0.6*(absMax-absMin))
        {
            FinishUp();
            running = false;
            return 1;

        }
    }
    justchanged=false;
    return 0;
}


void PIDTuner::FinishUp()
{
    *output = outputStart;
    //we can generate tuning parameters!
    Ku = 4*(2*oStep)/((absMax-absMin)*3.14159);
    Pu = (float)(peak1-peak2) / 1000;
}

float PIDTuner::GetKp()
{
    return controlType==1 ? 0.6 * Ku : 0.4 * Ku;
}

float PIDTuner::GetKi()
{
    return controlType==1? 1.2*Ku / Pu : 0.48 * Ku / Pu;  // Ki = Kc/Ti
}

float PIDTuner::GetKd()
{
    return controlType==1? 0.075 * Ku * Pu : 0;  //Kd = Kc * Td
}

void PIDTuner::SetOutputStep(float Step)
{
    oStep = Step;
}

float PIDTuner::GetOutputStep()
{
    return oStep;
}

void PIDTuner::SetControlType(int Type) //0=PI, 1=PID
{
    controlType = Type;
}

int PIDTuner::GetControlType()
{
    return controlType;
}

void PIDTuner::SetNoiseBand(float Band)
{
    noiseBand = Band;
}

float PIDTuner::GetNoiseBand()
{
    return noiseBand;
}

void PIDTuner::SetLookbackSec(float value)
{
    nLookBack = value*50;
    sampleTime = 20;
}

int PIDTuner::GetLookbackSec()
{
    return nLookBack * sampleTime / 1000;
}
