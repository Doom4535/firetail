#ifndef UTILITY_H_INCLUDED
#define UTILITY_H_INCLUDED

#include <stdint.h>

uint8_t set_bit(uint8_t bte, uint8_t bit, bool on);
uint8_t read_bit(uint8_t bte, uint8_t n);


#endif // UTILITY_H_INCLUDED
