#include "terminal.h"
#include "hil.h"
#include "CompleteGPS.h"
#include "firetail.h"


void radio_setup()
{
    cout << "~~~~~~Radio Setup~~~~~~~~~" << endl;
    cout << "\t 1 - Change air data rate" << endl;
}

void change_encryption_key()
{

}

void view_gps_data()
{
    cout << endl << "To exit GPS terminal, enter '@' and wait one second" << endl;
    chThdSleepMilliseconds(3000);
    while(true)
    {
        if(Serial.available())
        {
            char c = Serial.read();
            if(c == '@')
            {
                chThdSleepMilliseconds(1000);
                if(!Serial.available())
                    return;
            }
            GPS_PORT.print(c);
        }

        while(GPS_PORT.available())
            Serial.print(char(GPS_PORT.read()));

        Serial.flush();
    }
}

void term_func()
{
    chThdSleepMilliseconds(1000);
    cout << "~~~~~~~~~~Firetail UAV Terminal~~~~~~~~~~" << endl;

    while(true)
    {
        cout << "Main Menu" << endl;
        cout << "\t 1 - Radio (RFD900) setup" << endl;
        cout << "\t 2 - Change encryption key" << endl;
        cout << "\t 3 - View GPS data" << endl;
        cout << "\t 4 - Quit" << endl;
        cout << "Selection: ";


        while(Serial.available() == 0)
            chThdSleepMilliseconds(100);

        char buf[3];
        Serial.readBytesUntil('\n', buf, 3);
        char selection = buf[0];
        if(selection == '1')
            radio_setup();
        if(selection == '2')
            change_encryption_key();
        if(selection == '3')
            view_gps_data();
        if(selection == '4')
            return;

        cout << "\n";
    }
}


void term_iterate()
{
    if(hil_mode_on)
        return;

    if(Serial.available() >= 5)
    {
        char buf[6];
        Serial.readBytesUntil('\0', buf, 6);

        if(strncmp(buf, "term\n", 5) == 0)
        {
            term_func();
            cout << "Bye!" << endl;
        }
    }
}
