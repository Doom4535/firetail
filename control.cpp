/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "settings.h"
#include "firetail.h"
#include "coms.h"
#include "autotune.h"
#include "utility.h"

PIDController roll_controller;
PIDController pitch_controller;

PIDController vs_controller;
PIDController alt_controller;
CircularPIDController heading_controller;

PIDController throttle_controller;
FuzzySet speed_set;
ExpoMovingAvg speed_deriv;

float selected_vs;

int wp_turning; //zero for not turning, 1 for clockwise, 2 for anti-clockwise

static float dt;


AUTOLAUNCH_STATE autolaunch_state = AUTOLAUNCH_PRELAUNCH;
unsigned long launch_detect_ms = 0;
BrownLinearExpo launch_force_filter(0.1, 0);


static int ap_throttle_invert;
static int ap_elevator_invert;
static int ap_aileron_invert;
static int ap_rudder_invert;


//a no-frills bubble sorting algorithm for the action inference engine
static void bubble_sort(Firelink::ALARM_ACTION* actions)
{
    int n = 5;
    while(n != 0)
    {
        int newn = 0;
        for(int i = 1; i < n; i++)
        {
            if(actions[i-1] > actions[i])
            {
                Firelink::ALARM_ACTION tmp = actions[i];
                actions[i] = actions[i-1];
                actions[i-1] = tmp;
                newn = i;
            }
        }
        n = newn;
    }
}


void control_iteration(int16_t* control_in, int16_t* control_out, float delta_t)
{
    dt = delta_t;

    ap_elevator_invert = 1;
    if(read_bit(settings.autopilot_channel_invert, ELEVATOR_CHANNEL+1))
        ap_elevator_invert = -1;

    ap_aileron_invert = 1;
    if(read_bit(settings.autopilot_channel_invert, AILERON_CHANNEL+1))
        ap_aileron_invert = -1;

    ap_rudder_invert = 1;
    if(read_bit(settings.autopilot_channel_invert, RUDDER_CHANNEL+1))
        ap_rudder_invert = -1;

    ap_throttle_invert = 1;
    if(read_bit(settings.autopilot_channel_invert, THROTTLE_CHANNEL+1))
        ap_throttle_invert = -1;



    //create the list
    Firelink::ALARM_ACTION action_list[5];
    for(int i = 0; i < 5; i++)
        action_list[i] = Firelink::ALARM_ACTION_NONE;

    //add the possible actions to the list
    if((no_signal_alarm) && (ap_status.mode <= Firelink::AP_MODE_HNA))
        action_list[0] = settings.lost_control_action;

    if(boundary_alarm)
        action_list[1] = settings.boundary_action;

    if((no_gps_alarm) && (ap_status.mode > Firelink::AP_MODE_HNA))
        action_list[2] = settings.no_gps_action;

    if(low_bat_alarm)
        action_list[3] = settings.low_bat_action;

    if(no_datalink_alarm)
    {
        //if autopilot mode is not waypoints, or if the current waypoint isn't used (ie. mission complete)
        //more simply, if there is a mission in progress then it needs to continue regardless of telemetry link
        //other modes *should* have a telemetry link - except manual
        if((ap_status.mode != Firelink::AP_MODE_WAYPOINTS) || (!points[current_waypoint].used))
        {
            action_list[4] = settings.no_telem_action;

            //if there is no telemetry and no RC control
            if((settings.no_telem_action == Firelink::ALARM_ACTION_MANUAL_CTRL) && (no_signal_alarm))
                action_list[4] = Firelink::ALARM_ACTION_RTB; //fly it back home. it's better to crash into the operator than a bystander
        }
        //likewise, if the autopilot mode is manual and there is a RC signal, the datalink is not required
        if((ap_status.mode == Firelink::AP_MODE_MANUAL) && (!no_signal_alarm))
            action_list[4] = Firelink::ALARM_ACTION_NONE;
    }

    //remove illogical actions
    if(no_gps_alarm) //if there is no GPS lock, we cannot navigate home or loiter. the best we can do is a fixed bank loiter
    {
        for(int i = 0; i < 4; i++)
        {
            if(action_list[i] >= Firelink::ALARM_ACTION_RTB)
                action_list[i] = Firelink::ALARM_ACTION_FIXED_BANK_LOITER;
        }
        //if there is no GPS we can't determine if the boundary has been hit either
        if(boundary_alarm)
            action_list[1] = Firelink::ALARM_ACTION_NONE;
    }

    //sort the list so the most failure tolerant action comes first
    bubble_sort(action_list);

    Firelink::ALARM_ACTION action_selection = Firelink::ALARM_ACTION_NONE;
    //find the first action
    for(int i = 0; i < 5; i++)
    {
        if(action_list[i] != Firelink::ALARM_ACTION_NONE)
        {
            action_selection = action_list[i];
            break;
        }
    }

    if(action_selection != last_action)
    {
        if(action_selection == Firelink::ALARM_ACTION_LOITER)
        {
            selected_alt = inst.baro_alt + 250;
            loiter_pos = make_coord(inst.latitude, inst.longitude);
        }
        if(action_selection == Firelink::ALARM_ACTION_RTB)
        {
            rtb_mode = false; //false for path following
            from_point.pos = make_coord(inst.latitude, inst.longitude);
        }
        last_action = action_selection;
    }

    if(action_selection != Firelink::ALARM_ACTION_NONE)
    {
        switch(action_selection)
        {
        case Firelink::ALARM_ACTION_LOCKUP:
        {
            control_out[0] = control_out[1] = control_out[2] = control_out[3] = -127;
            control_out[THROTTLE_CHANNEL] = -127*ap_throttle_invert;
        }
        break;
        case Firelink::ALARM_ACTION_NO_PWR_GLIDE:
        {
            //wings level, gentle pitch down, no throttle
            ap_status.pitch = -500;
            ap_status.roll = 0;
            pitch_and_roll_control(control_out);
            control_out[THROTTLE_CHANNEL] = -127*ap_throttle_invert;
        }
        break;
        case Firelink::ALARM_ACTION_FIXED_BANK_LOITER:
        {
            fixed_bank_loiter(control_out);
            control_out[THROTTLE_CHANNEL] = settings.at_cruise_power*ap_throttle_invert;
        }
        break;
        case Firelink::ALARM_ACTION_RTB:
        {
            return_to_base(control_out);
            //we're doing a return to base, so we must have a GPS lock. autothrottle is safe to use here.
            control_out[THROTTLE_CHANNEL] = autothrottle_with_vert_speed()*ap_throttle_invert;
        }
        break;
        case Firelink::ALARM_ACTION_LOITER:
        {
            orbit(control_out, true);
            control_out[THROTTLE_CHANNEL] = autothrottle_with_vert_speed()*ap_throttle_invert;
        }
        break;
        case Firelink::ALARM_ACTION_MANUAL_CTRL:
        {
            control_out[ELEVATOR_CHANNEL] = control_in[ELEVATOR_CHANNEL];
            control_out[AILERON_CHANNEL] = control_in[AILERON_CHANNEL];
            control_out[RUDDER_CHANNEL] = control_in[RUDDER_CHANNEL];
            control_out[THROTTLE_CHANNEL] = control_in[THROTTLE_CHANNEL];
        }
        break;
        }
        return;
    }

    //if none of the emergency control functions have overriden the controls, then just do a normal control iteration
    //autopilot time

/*
    if(vd.autolaunch_on)
    {
        autolaunch(control_out);
        return;
    }
*/

    switch(ap_status.mode)
    {


        /**
        AP_MODE_TRAINER -
        This mode allows for near-complete manual control, except it does not allow the aircraft to become inverted.
        It will not allow the aircraft to exceed extreme pitch or bank angles.
        It will not allow the aircraft below 120ft from home altitude.
        Below 120ft it will go pitch up, wings level.
        Hmmm, possibly a good auto-takeoff mode?
        */

    case Firelink::AP_MODE_TRAINER:
    {
        control_out[ELEVATOR_CHANNEL] = control_in[ELEVATOR_CHANNEL];
        control_out[AILERON_CHANNEL] = control_in[AILERON_CHANNEL];
        control_out[RUDDER_CHANNEL] = control_in[RUDDER_CHANNEL];
        control_out[THROTTLE_CHANNEL] = control_in[THROTTLE_CHANNEL];

        if(inst.pitch > 4500)
            control_out[ELEVATOR_CHANNEL] = 127*ap_elevator_invert;
        if(inst.pitch < -4500)
            control_out[ELEVATOR_CHANNEL] = -127*ap_elevator_invert;

        if(inst.roll > 9000)
            control_out[AILERON_CHANNEL] = 127*ap_aileron_invert;
        if(inst.roll < -9000)
            control_out[AILERON_CHANNEL] = -127*ap_aileron_invert;


        bool thr_set = false; //throttle set flag

        if((inst.baro_alt - home.alt) < 120) //if it's less than 120ft above the home altitude
        {
            ap_status.pitch = settings.max_pitch_up; //pitch up
            ap_status.roll = 0; //wings level

            pitch_and_roll_control(control_out);

            if(autothrottle_on)
            {
                control_out[THROTTLE_CHANNEL] = 127*ap_throttle_invert;
                thr_set = true;
            }
        }
        if(!thr_set)
        {
            if(autothrottle_on)
                control_out[THROTTLE_CHANNEL] = autothrottle_with_pitch()*ap_throttle_invert;
            else
                control_out[THROTTLE_CHANNEL] = control_in[THROTTLE_CHANNEL];
        }
    }
    break;

    /**
    AP_MODE_PITCH_AND_ROLL
    In this mode the aircraft will follow a selected pitch and bank angle. These angles are selected by the controller.
    The elevator control becomes the pitch control and the aileron control becomes the roll angle control.
    Throttle is manually controlled, unless autothrottle is on.
    */
    case Firelink::AP_MODE_PNR:
    {
        ap_status.pitch = control_in[ELEVATOR_CHANNEL] * -75;
        ap_status.roll = control_in[AILERON_CHANNEL] * -110;

        pitch_and_roll_control(control_out);

        if(autothrottle_on)
            control_out[THROTTLE_CHANNEL] = autothrottle_with_pitch()*ap_throttle_invert;
        else
            control_out[THROTTLE_CHANNEL] = control_in[THROTTLE_CHANNEL];
    }
    break;

    /**
    AP_MODE_HNA
    The controller selects a heading and altitude.
    */
    case Firelink::AP_MODE_HNA:
    {
        //check that control input isn't close to zero
        //the Rx transmitter is never exactly 1500ms with hands off sticks
        //so this prevents the selected heading from slowly moving around without operator input
        if(abs(control_in[AILERON_CHANNEL]) > 15)
            ap_status.heading += control_in[AILERON_CHANNEL] * -heading_control_rate; //now rotate selected heading

        if(abs(control_in[ELEVATOR_CHANNEL]) > 15)
            //max commanded rate of climb would be 96 feet per second (about 6,000fpm). i'd like to see your foamy do that
            selected_alt += float(control_in[ELEVATOR_CHANNEL]) * -(altitude_control_rate*0.1);

        if(ap_status.heading > 18000)
            ap_status.heading -= 36000;
        if(ap_status.heading < -18000)
            ap_status.heading += 36000;

        heading_and_alt_control(control_out);

        if(autothrottle_on)
            control_out[THROTTLE_CHANNEL] = autothrottle_with_vert_speed()*ap_throttle_invert;
        else
            control_out[THROTTLE_CHANNEL] = control_in[THROTTLE_CHANNEL];
    }
    break;

    /**
    AP_MODE_LOITER
    In this mode the aircraft will fly in a circle over a specified location.
    This mode is completely autonomous and requires no controller input.
    Throttle is automatically controlled to maintain ground speed.
    */
    case Firelink::AP_MODE_LOITER:
    {
        if(abs(control_in[ELEVATOR_CHANNEL]) > 15)
            selected_alt += float(control_in[ELEVATOR_CHANNEL]) * -(altitude_control_rate*0.1);

        orbit(control_out, true);
        control_out[THROTTLE_CHANNEL] = autothrottle_with_vert_speed()*ap_throttle_invert;
    }
    break;

    /**
    AP_MODE_RETURN_TO_BASE
    The aircraft will return to base and loiter over it at 250ft
    This mode is completely autonomous and requires no controller input.
    Throttle control is automatic.
    */
    case Firelink::AP_MODE_RTB:
    {
        return_to_base(control_out);
        control_out[THROTTLE_CHANNEL] = autothrottle_with_vert_speed()*ap_throttle_invert;
    }
    break;

    /**
    AP_MODE_WAYPOINTS
    The aircraft will fly along a series of waypoints. At the end it will loiter.
    This mode is completely autonomous and requires no controller input.
    Throttle control is automatic.
    */
    case Firelink::AP_MODE_WAYPOINTS:
    {
        waypoints(control_out);
        control_out[THROTTLE_CHANNEL] = autothrottle_with_vert_speed()*ap_throttle_invert;
    }
    break;

    /**
    If the mode is unknown, assume it's in manual.
    If it isn't in AP_MODE_MANUAL, then the sun has probably collided with planet Earth.
    And Hell has recently received a good foot of snow.
    */
    default:
    {
        control_out[ELEVATOR_CHANNEL] = control_in[ELEVATOR_CHANNEL];
        control_out[AILERON_CHANNEL] = control_in[AILERON_CHANNEL];
        control_out[RUDDER_CHANNEL] = control_in[RUDDER_CHANNEL];

        if(autothrottle_on)
            control_out[THROTTLE_CHANNEL] = autothrottle_with_pitch()*ap_throttle_invert;
        else
            control_out[THROTTLE_CHANNEL] = control_in[THROTTLE_CHANNEL];
    }
    break;
    }
}


void pitch_and_roll_control(int16_t* out)
{
    //apply rate limits to selected pitch & roll. this really helps smooth out high frequency oscillations from the heading & alt controllers.
    //plus it helps make aerial photography turn out better
    int16_t diff_ap_roll = ap_status.roll - selected_roll;
    selected_roll += constrain(diff_ap_roll, -settings.max_roll_rate*5, settings.max_roll_rate*5);

    int16_t diff_ap_pitch = ap_status.pitch - selected_pitch;
    selected_pitch += constrain(diff_ap_pitch, -settings.max_pitch_rate*5, settings.max_pitch_rate*5);

    //apply some upwards pitch if the aircraft is in a turn
    //this helps counteract the loss of lift and subsequent loss of altitude during turns
    float old_pitch_integral = pitch_controller.integral;

    float ec = (abs(RAD_TO_DEG*(tan(DEG_TO_RAD*(inst.roll/100.0)))
                    * settings.pitch_coupling))*100;


    //calculate 3D speed
    float gnd_spd = systems.ground_speed/100.0f;
    float vert_spd = inst.vert_speed/100.0f*FPS_TO_KMH;
    float speed_3d = sqrt((gnd_spd*gnd_spd) + (vert_spd*vert_spd));
    if(speed_3d < 10.0f)
        speed_3d = 10.0f;

    //scale gains based on velocity
    //the faster the plane travels through the air, the more responsive the controls are
    //scaling down gains at higher airspeeds helps keep the controller stable
    //this is called gain scheduling
    pitch_controller.Kp = (settings.pitch_pid.kp*60.0f)/speed_3d;
    pitch_controller.Ki = (settings.pitch_pid.ki*60.0f)/speed_3d;
    pitch_controller.Kd = (settings.pitch_pid.kd*60.0f)/speed_3d;

    roll_controller.Kp = (settings.roll_pid.kp*60.0f)/speed_3d;
    roll_controller.Ki = (settings.roll_pid.ki*60.0f)/speed_3d;
    roll_controller.Kd = (settings.roll_pid.kd*60.0f)/speed_3d;

    float elevator_out = -pitch_controller.step(
                             float(selected_pitch) + ec, float(inst.pitch), dt);//, ahrs_raw_ang_vel().y(), true);



    if(systems.ground_speed < 500) //if ground speed is less than 5km/h or motor isn't on
        pitch_controller.integral = old_pitch_integral; //prevent the integral from winding up

    /*
        TODO
        stall and overspeed protection. probably not worth doing without an airspeed sensor.

        //push the nose down if a stall is approached
        //elevator_out += speed_range.crisp_out(vd.ground_speed/100.0);
    */
    out[ELEVATOR_CHANNEL] = elevator_out*ap_elevator_invert;

    float old_roll_integral = roll_controller.integral;
    out[AILERON_CHANNEL] = -roll_controller.step(float(selected_roll), float(inst.roll), dt)*ap_aileron_invert;//, ahrs_raw_ang_vel().z(), true);
    if(systems.ground_speed< 500)
        roll_controller.integral = old_roll_integral;

    out[RUDDER_CHANNEL] = out[AILERON_CHANNEL]*(settings.yaw_coupling*-0.1)*ap_rudder_invert;

    /*
        fselected_pitch = selected_pitch/100.0;
        factual_pitch = vd.pitch/100.0;
        pitch_tuner.SetNoiseBand(1.0);
        pitch_tuner.SetLookbackSec(1.0);
        pitch_tuner.SetControlType(0);

        if(pitch_tuner.Runtime())
        {
            pitch_controller.Kp = (pitch_controller.Kp*0.8) + (pitch_tuner.GetKp()/100.0*0.2);
            pitch_controller.Ki = (pitch_controller.Ki*0.8) + (pitch_tuner.GetKi()/100.0*0.2);
            pitch_controller.Kd = pitch_controller.Kp*0.08;
        }
    */

}


void heading_and_alt_control(int16_t* out)
{
    //if we're below selected altitude and decending
    //then the aircraft is seriously mistuned and could crash
    //so ignore the PID controller and just pitch up to climb
    if(((ap_status.alt - inst.baro_alt) > 100) && (inst.vert_speed < 0))
    {
        ap_status.pitch = settings.max_pitch_up;
        alt_controller.integral = 0;
    }
    else
    {
        selected_vs = constrain(alt_controller.step(
                             float(selected_alt), float(ad_pressure_alt()), dt),
                             float(-settings.max_decent_rate),
                             float(settings.max_climb_rate));

        ap_status.pitch = constrain(vs_controller.step(selected_vs, ad_vert_speed(), dt),
                            float(-settings.max_pitch_down), float(settings.max_pitch_up));
    }


    while(ap_status.heading > 18000)
        ap_status.heading -= 36000;
    while(ap_status.heading < -18000)
        ap_status.heading += 36000;

    ap_status.roll = -constrain(heading_controller.step(
                           float(systems.ground_track), float(ap_status.heading), dt),
                           float(-settings.max_bank_angle),
                           float(settings.max_bank_angle));

    pitch_and_roll_control(out);
}

void orbit(int16_t* out, bool clockwise)
{
    ap_status.wp_lat = loiter_pos.lat*1000000;
    ap_status.wp_lng = loiter_pos.lng*1000000;

    float dist_to_lp = distance_between(make_coord(inst.latitude, inst.longitude), loiter_pos); //distance to loiter point
    float bearing_to = bearing_from_to(make_coord(inst.latitude, inst.longitude), loiter_pos)*100; //bearing to loiter point

    //but if we are outside the radius, we need to rotate towards the loiter point
    float err = (settings.loiter_radius - dist_to_lp) * (settings.loiter_correction/1000.0);
    err = RAD_TO_DEG*(atan(err))*100;
    if(clockwise)
        ap_status.heading = bearing_to - 9000 - err;
    else
        ap_status.heading = bearing_to + 9000 + err;

    heading_and_alt_control(out);
}


void return_to_base(int16_t* out)
{
    if(rtb_mode) //if we are in loiter mode (ie, reached home base)
    {
        selected_alt = home.alt + 350;
        orbit(out, true);
    }
    else //otherwise, keep following the path home
    {
        ap_status.wp_lat = home.pos.lat*1000000;
        ap_status.wp_lng = home.pos.lng*1000000;
        //if we're inside the loiter radius, switch to loitering instead of path following
        if(distance_between(make_coord(inst.latitude, inst.longitude), from_point.pos) < settings.loiter_radius)
        {
            loiter_pos = home.pos;
            rtb_mode = true; //switch to loitering logic
        }

        float path_direction = bearing_from_to(from_point.pos, home.pos);
        float xtrack_dist = cross_track_distance(from_point.pos, home.pos, make_coord(inst.latitude, inst.longitude));

        if(fabs(xtrack_dist) < 1000)
            xtrack_integral -= xtrack_dist;
        xtrack_der.step((xtrack_dist - last_xtrack_dist)/dt);
        last_xtrack_dist = xtrack_dist;
        float xtrack_correction = atan(
                                      ((settings.path_pid.kp * xtrack_dist) +
                                       (settings.path_pid.ki * xtrack_integral/1000.0) +
                                       (settings.path_pid.kd * xtrack_der.get()))
                                      / 1000.0);

        ap_status.heading = (path_direction - RAD_TO_DEG*(xtrack_correction))*100;
        heading_and_alt_control(out);
    }
}


void waypoints(int16_t* out)
{
    waypoint a, b;
    b = points[current_waypoint];
    if(!b.used)
    {
        orbit(out, true);
        return;
    }

    ap_status.wp_lat = b.pos.lat*1000000;
    ap_status.wp_lng = b.pos.lng*1000000;
    ap_status.wp_number = current_waypoint;

    a = from_point;

    if(distance_between(make_coord(inst.latitude, inst.longitude), a.pos) > distance_between(a.pos, b.pos))
    {
        //we've reached the distance, but can't jump to the next wp unless we're at the correct altitude
        wp_hit = true;
    }
    if(wp_hit)
    {
        //once we're at the waypoint, loiter around it until the correct altitude is reached
        //if we're within 25ft
        if(abs(inst.baro_alt - (b.alt+home.alt)) < 25)
        {
            //move along to the next waypoint
            from_point = b;
            loiter_pos = b.pos;
            selected_alt = b.alt + home.alt;
            current_waypoint++;
            xtrack_integral = 0.0;
            if(!points[current_waypoint].used)
            {
                loiter_pos = b.pos;
                selected_alt = b.alt + home.alt; //home altitude is the reference point for altitude
            }
            //reset flag
            wp_hit = false;

            //once the waypoint has been hit, we'll do a nice turn to get on track for the next point
            //we'll use orbiting logic during the turn, but only if the turn is more than 45 degrees
            //first to find out to turn clockwise or anticlockwise
            float desired_track = bearing_from_to(from_point.pos, points[current_waypoint].pos);
            float track_diff = desired_track - (systems.ground_track/100.0f);

            while(track_diff < -180.0f)
                track_diff += 360.0f;
            while(track_diff > 180.0f)
                track_diff -= 360.0f;

            //if difference between desired track and current track is greater than zero
            //then we have to turn clockwise
            //else turn anticlockwise
            if(track_diff > 4500)
                wp_turning = 1; //1 for clockwise
            if(track_diff < -4500)
                wp_turning = 2; //2 for anticlockwise

            waypoints(out);
            return;
        }
        loiter_pos = b.pos;
        orbit(out, true);
        return;
    }

    float path_direction = bearing_from_to(a.pos, b.pos);
    float xtrack_dist = cross_track_distance(a.pos, b.pos, make_coord(inst.latitude, inst.longitude));


    if(wp_turning)
    {
        float track_diff = path_direction - (systems.ground_track/100.0f);

        while(track_diff < -180.0f)
            track_diff += 360.0f;
        while(track_diff > 180.0f)
            track_diff -= 360.0f;

        if(wp_turning == 1)
        {
            orbit(out, true);

            if(track_diff < 0)
                wp_turning = 0;
        }
        if(wp_turning == 2)
        {
            orbit(out, false);
            if(track_diff > 0)
                wp_turning = 0;
        }
        return;
    }

    if(fabs(xtrack_dist) < 1000)
        xtrack_integral += xtrack_dist;
    xtrack_der.step((xtrack_dist - last_xtrack_dist)/dt);
    last_xtrack_dist = xtrack_dist;

    float xtrack_correction = atan(
                                  ((settings.path_pid.kp * xtrack_dist) +
                                   (settings.path_pid.ki * xtrack_integral/1000.0) +
                                   (settings.path_pid.kd * xtrack_der.get()))
                                  / 1000.0);

    xtrack_correction = RAD_TO_DEG*(xtrack_correction)*100;
    xtrack_correction = constrain(xtrack_correction, -6000, 6000);

    int ixtk = xtrack_correction;
    ap_status.heading = (path_direction*100) - ixtk;
    selected_alt = b.alt + home.alt;
    heading_and_alt_control(out);
}


void autolaunch(int16_t* outs)
{
    //TODO
    switch(autolaunch_state)
    {
    case AUTOLAUNCH_PRELAUNCH:
    {
        imu::Vector<3> world_accel = ahrs_world_acceleration();
        world_accel.z() = 0;
        launch_force_filter.step(world_accel.magnitude());

        if(launch_force_filter.get() > 15.0) //if acceleration is greater than the threshold m/s/s
        {
            autolaunch_state = AUTOLAUNCH_DETECT; //a throw has been detected
            launch_detect_ms = ms();
        }
        outs[THROTTLE_CHANNEL] = -128;
    }
    break;
    case AUTOLAUNCH_DETECT:
    {

        if((ms() - launch_detect_ms) > 750) //half a second after launch detect
        {
            //TODO make detect speed changeable
            if(gps.speed > 800) //check to see if GPS speed is greater than 8km/h
            {
                autolaunch_state = AUTOLAUNCH_ROLLING;
                ap_status.heading = inst.heading; //assume we're aligned on runway, so keep on this heading
            }
            else
                autolaunch_state = AUTOLAUNCH_PRELAUNCH;
        }

        ap_status.pitch = 0;
        ap_status.roll = 0;
        pitch_and_roll_control(outs);
        outs[THROTTLE_CHANNEL] = -128;
    }
    break;
    case AUTOLAUNCH_ROLLING:
    {
        //TODO control heading with rudder / nose-wheel steering
        ap_status.pitch = 0;
        ap_status.roll = 0;
        pitch_and_roll_control(outs);
        if(systems.ground_speed >= 2000) //TODO make this change-able
            autolaunch_state = AUTOLAUNCH_ROTATE;

        outs[THROTTLE_CHANNEL] = 128;
    }
    break;
    case AUTOLAUNCH_ROTATE:
    {
        ap_status.pitch = constrain(vs_controller.step(float(settings.max_climb_rate), ad_vert_speed(), dt),
                            float(-settings.max_pitch_up), 0);

        ap_status.roll = -constrain(heading_controller.step(
                               float(inst.heading), float(ap_status.heading), dt),
                               float(-settings.max_bank_angle),
                               float(settings.max_bank_angle));

        pitch_and_roll_control(outs);

        if((inst.baro_alt - home.alt) > 100)
            autolaunch_on = false;
        outs[THROTTLE_CHANNEL] = 128;
    }
    break;
    }
}

void fixed_bank_loiter(int16_t* outs)
{
    ap_status.pitch = constrain(alt_controller.step(selected_alt, inst.baro_alt, dt),
                            float(-settings.max_pitch_down),
                            float(settings.max_pitch_up));
    ap_status.roll = -1000;
    pitch_and_roll_control(outs);
}



static float at_integ = 0;


int16_t autothrottle_with_pitch()
{
    int offset = (settings.at_cruise_power-50)*2.54;

    float sb = speed_set.crisp_out((systems.ground_speed/100.0)-(settings.at_cruise_speed));
    float error = sb + (inst.pitch*settings.at_settings_pt/50.0);
    at_integ = constrain(at_integ+error, -5000, 5000);

    float pid_out = constrain((settings.autothrottle.kp * error +
                               (settings.autothrottle.ki/100.0) * at_integ +
                               (settings.autothrottle.kd/100.0) * gps_acceleration.get() +
                               offset),
                              settings.throttle_min, settings.throttle_max);

    float min_set = (settings.throttle_max - settings.throttle_min);
    min_set /= 100.0;
    min_set *= settings.min_thr_setting;
    min_set -= 128.0;

    float result = constrain(pid_out, min_set, settings.throttle_max);
    return result;
}


int16_t autothrottle_with_vert_speed()
{
    int offset = (settings.at_cruise_power-50)*2.54;

    float sb = speed_set.crisp_out((systems.ground_speed/100.0)-(settings.at_cruise_speed));

    float error = sb + (selected_vs*5.0*settings.at_settings_pt);

    float pid_out = constrain((settings.autothrottle.kp * error +
                               (settings.autothrottle.ki/100.0) * at_integ +
                               (settings.autothrottle.kd/100.0) * gps_acceleration.get() +
                               offset),
                              settings.throttle_min, settings.throttle_max);

    float min_set = (settings.throttle_max - settings.throttle_min);
    min_set /= 100.0;
    min_set *= settings.min_thr_setting;
    min_set -= 128.0;

    float result = constrain(pid_out, min_set, settings.throttle_max);

    if((pid_out < settings.throttle_max) && (pid_out > min_set))
        at_integ = constrain(at_integ+error, -5000, 5000);

    return result;
}

