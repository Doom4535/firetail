/**

NOTICE: This file has been automatically generated from firetail.xml

Unless explicit and written permission is received from the copyright
holder, all files generated by the Firelink-2.0 code generator must remain 
under the MIT License and the copyright notices must not be modified or 
removed. 


The MIT License (MIT)

Copyright (c) 2014 Firetail UAV Systems

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


**/


#ifndef RING_BUF_H
#define RING_BUF_H

#include <stdint.h>

template <class T> class RingBuffer
{
public:
    RingBuffer(T* buffer, uint16_t sz)
    {
        size = sz;
        start = 0;
        end = 0;
        buf = buffer;
    }

	bool is_full()
	{
        return (end + 1) % size == start;
	}

	uint16_t available()
	{
	    return (uint16_t)(size + end - start) % size;
	}

	void put(T b)
	{
        if(is_full())
            return;
        buf[end] = b;
        end = (end + 1) % size;
	}

	T get()
	{
	    T ret = buf[start];
        start = (start + 1) % size;
        return ret;
	}

	T peek_ahead(uint16_t n=0)
	{
	    uint16_t pos = (start+n) % size;
        return buf[pos];
	}

	void empty()
	{
	    start = 0;
        end = 0;
	}

private:
	uint16_t size;
    uint16_t start;
    uint16_t end;
    T* buf;
};


#endif // RING_BUF_H
