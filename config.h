/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FIRETAIL_BUILD_CONFIG_H
#define FIRETAIL_BUILD_CONFIG_H

#include <Arduino.h>
#include <SdFat.h>

///BUILD CONFIGURATION


///RC CONTROLLER INPUT PIN MAPPINGS
#define AILERON_SIGNAL_IN 		23
#define ELEVATOR_SIGNAL_IN 		22
#define THROTTLE_SIGNAL_IN 		21
#define RUDDER_SIGNAL_IN 		20
#define SERVO_5_IN              17
#define SERVO_6_IN              16


///SERVO OUTPUT PIN MAPPINGS
#define AILERON_SERVO_OUT		2
#define ELEVATOR_SERVO_OUT		3
#define THROTTLE_SERVO_OUT		4
#define RUDDER_SERVO_OUT		5
#define SERVO_5_OUT             6
#define SERVO_6_OUT             14

///I2C BUS
#define I2C_BUS					Wire

///SD CARD SS PIN
#define SD_CARD_SS_PIN			15

///SERIAL PORTS
///flightgear/HIL port
#define FG_PORT					Serial
#define GPS_PORT				Serial3
///firelink port
#define FL1_PORT				Serial1
///firelink2 port
#define FL2_PORT				Serial2

///CURRENT AND VOLTAGE SENSOR PINS
#define CURRENT_PIN				A11
#define VOLTAGE_PIN				A10

///control loop rate in hertz
#define ITERATION_RATE			50.0
///END CONFIGURATION



#define ITERATION_DELAY (1 / ITERATION_RATE)
#define ITERATION_DELAY_MS (ITERATION_DELAY*1000)


#define AILERON_CHANNEL 0
#define ELEVATOR_CHANNEL 1
#define THROTTLE_CHANNEL 2
#define RUDDER_CHANNEL 3
#define SERVO_5_CHANNEL 4
#define SERVO_6_CHANNEL 5


extern ArduinoOutStream cout;

#endif

