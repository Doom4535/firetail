#include "current_sensor.h"

#include "../filters.h"

CurrentSensor::CurrentSensor(int pin)
{
	apin = pin;
	filter.set_factor(0.01);
}

	//returns amps
float CurrentSensor::readAmps()
{
	//convert from ADC units to volts
	float z = (readRaw()-bias)/310.3;
	z /= sens;
	filter.step(z);
	return filter.get();
}

int CurrentSensor::readRaw()
{
	return analogRead(apin);
}

//sets the bias in ADC units
void CurrentSensor::set_bias(int b)
{
	bias = b;
}

	//sensor sensitivity in millivolts per amp
void CurrentSensor::set_sensitivity(int mvpa)
{
	sens = (mvpa/1000.0);
}


