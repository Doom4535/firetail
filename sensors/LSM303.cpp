/*
    FireTail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "LSM303.h"
#include "../i2c_t3.h"
#include <math.h>

#define D_SA0_HIGH_ADDRESS              0b0011101
#define D_SA0_LOW_ADDRESS               0b0011110


LSM303::LSM303(void)
{

    _device = LSM303_DEVICE_AUTO;
    //acc_address = detectSA0_A();

    io_timeout = 0;  // 0 = no timeout
    did_timeout = false;
}

// Public Methods //////////////////////////////////////////////////////////////

void LSM303::print_acc()
{
	Serial.print(float(a.x()));
	Serial.print(" ");
	Serial.print(float(a.y()));
	Serial.print(" ");
	Serial.println(float(a.z()));
}

void LSM303::print_mag()
{
	Serial.print(float(m.x()));
	Serial.print(" ");
	Serial.print(float(m.y()));
	Serial.print(" ");
	Serial.println(float(m.z()));
}

bool LSM303::timeoutOccurred()
{
    return did_timeout;
}

void LSM303::setTimeout(unsigned int timeout)
{
    io_timeout = timeout;
}

unsigned int LSM303::getTimeout()
{
    return io_timeout;
}

void LSM303::init(byte device, byte sa0_a)
{
    acc_address = 0x1D;
}

// Turns on the LSM303's accelerometer and magnetometers and places them in normal
// mode.
void LSM303::enableDefault(void)
{
    // 0x57 = 0b01010111
    // AFS = 0 (+/- 8 g full scale)

    writeReg(LSM303_CTRL_REG0_A, 0b01000000);
    writeReg(LSM303_CTRL_REG1_A, 0b10100111);
    writeReg(LSM303_CTRL_REG2_A, 0b00011000);
    writeReg(LSM303_FIFO_CTRL_REG_A, 0b01000000);

    // Magnetometer

    // 0x64 = 0b01100100
    // M_RES = 11 (high resolution mode); M_ODR = 001 (6.25 Hz ODR)
    writeReg(0x24, 0x64);

    // 0x20 = 0b00100000
    // MFS = 01 (+/- 4 gauss full scale)
    writeReg(0x25, 0x20);

    // 0x00 = 0b00000000
    // MLP = 0 (low power mode off); MD = 00 (continuous-conversion mode)
    writeReg(0x26, 0x00);
}

void LSM303::writeReg(byte reg, byte value)
{
    Wire.beginTransmission(acc_address);
    Wire.write(reg);
    Wire.write(value);
    last_status = Wire.endTransmission();
}

byte LSM303::readReg(byte reg)
{
    byte value;

    Wire.beginTransmission(acc_address);
    Wire.write(reg);
    last_status = Wire.endTransmission();
    Wire.requestFrom(acc_address, (byte)1);
    value = Wire.read();
    Wire.endTransmission();

    return value;
}


// Reads the 3 accelerometer channels and stores them in vector a
imu::Vector<3> LSM303::read_acc(void)
{
	io_timeout = 5;
    Wire.beginTransmission(acc_address);
    // assert the MSB of the address to get the accelerometer
    // to do slave-transmit subaddress updating.
    Wire.write(LSM303_OUT_X_L_A | (1 << 7));
    last_status = Wire.endTransmission();
    Wire.requestFrom(acc_address, (byte)6);

    unsigned int millis_start = millis();
    did_timeout = false;
    while (Wire.available() < 6) {
        if (io_timeout > 0 && ((unsigned int)millis() - millis_start) > io_timeout) {
            did_timeout = true;
            return imu::Vector<3>();
        }
    }

    byte xla = Wire.read();
    byte xha = Wire.read();
    byte yla = Wire.read();
    byte yha = Wire.read();
    byte zla = Wire.read();
    byte zha = Wire.read();

    // combine high and low bytes, then shift right to discard lowest 4 bits (which are meaningless)
    // GCC performs an arithmetic right shift for signed negative numbers, but this code will not work
    // if you port it to a compiler that does a logical right shift instead.
    a.x() = (int16_t)(xha << 8 | xla);
    a.y() = (int16_t)(yha << 8 | yla);
    a.z() = (int16_t)(zha << 8 | zla);

    a = a * 0.000244148f;
	return a;
}

// Reads the 3 magnetometer channels and stores them in vector m
imu::Vector<3> LSM303::read_mag(void)
{
    Wire.beginTransmission(acc_address);
    Wire.write(D_OUT_X_L_M | (1 << 7));
    last_status = Wire.endTransmission();
    Wire.requestFrom(int(acc_address), int(6));

	io_timeout = 5;
    unsigned int millis_start = millis();
    did_timeout = false;
    while (Wire.available() < 6) {
        if (io_timeout > 0 && ((unsigned int)millis() - millis_start) > io_timeout) {
            did_timeout = true;
            return imu::Vector<3>();
        }
    }

    byte xlm = Wire.read();
    byte xhm = Wire.read();

    byte yhm, ylm, zhm, zlm;


    // DLM, DLHC: register address for Z comes before Y
    ylm = Wire.read();
    yhm = Wire.read();
    zlm = Wire.read();
    zhm = Wire.read();


    // combine high and low bytes
    m.x() = (int16_t)(xhm << 8 | xlm);
    m.y() = (int16_t)(yhm << 8 | ylm);
    m.z() = (int16_t)(zhm << 8 | zlm);

	return m;
}

// Reads all 6 channels of the LSM303 and stores them in the object variables
void LSM303::read(void)
{
    read_acc();
    read_mag();
}

// Private Methods //////////////////////////////////////////////////////////////

byte LSM303::detectSA0_A(void)
{

    Wire.beginTransmission(D_SA0_LOW_ADDRESS);
    Wire.write(LSM303_CTRL_REG1_A);
    last_status = Wire.endTransmission();
    Wire.requestFrom(D_SA0_LOW_ADDRESS, 1);
    if (Wire.available())
    {
        Wire.read();
        return LSM303_SA0_A_LOW;
    }
    else
        return LSM303_SA0_A_HIGH;

}
