#ifndef CURRENT_SENSOR_H
#define CURRENT_SENSOR_H

#include <Arduino.h>
#include "../filters.h"

class CurrentSensor
{
public:
	CurrentSensor(int pin);

	//returns amps
	float readAmps();
	
	//return raw ADC reading
	int readRaw();

	//sets the bias in ADC units
	void set_bias(int bias);

	//sensor sensitivity in millivolts per amp
	void set_sensitivity(int mvpa);

private:
	int apin;

	int bias;
	float sens;

	ExpoMovingAvg filter;
};


#endif

