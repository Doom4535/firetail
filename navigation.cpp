/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "navigation.h"
#include "conversions.h"
#include <string.h>

coord make_coord(int32_t lat, int32_t lng)
{
    coord ret;
    ret.lat = lat/1000000.0;
    ret.lng = lng/1000000.0;
    return ret;
}

coord make_coordf(float lat, float lng)
{
    coord ret;
    ret.lat = lat;
    ret.lng = lng;
    return ret;
}

float bearing_from_to(coord from, coord to)
{
    //1 = from, 2 = to
    //Unnamed but amazing mathematics from
    //http://www.movable-type.co.uk/scripts/latlong.html

    //convert to radians firstly
    from.lat *= DEG_TO_RAD;
    from.lng *= DEG_TO_RAD;

    to.lat *= DEG_TO_RAD;
    to.lng *= DEG_TO_RAD;

    float dLon = to.lng - from.lng;
    float y = sin(dLon) * cos(to.lat);
    float x = cos(from.lat)*sin(to.lat) -
              sin(from.lat)*cos(to.lat)*cos(dLon);
    return RAD_TO_DEG*(atan2(y, x));
}

float distance_between(coord a, coord b)
{
    //The Spherical Law Of Cosines
    //ref: http://www.movable-type.co.uk/scripts/latlong.html

    a.lat *= DEG_TO_RAD;
    a.lng *= DEG_TO_RAD;

    b.lat *= DEG_TO_RAD;
    b.lng *= DEG_TO_RAD;

    //this functions multiplies the result by 1000 to get the answer in meters, rather than kilometers
    return acos(sin(a.lat)*sin(b.lat) + cos(a.lat)*cos(b.lat)*cos(b.lng-a.lng)) * 6371 * 1000;
}

float cross_track_distance(coord from, coord to, coord here)
{
    //Cross track distance function, also from
    //http://www.moveable-type.co.uk/scripts/latlong.html

    float d13, brng13, brng12;
    d13 = distance_between(from, here);
    brng13 = DEG_TO_RAD*(bearing_from_to(from, here));
    brng12 = DEG_TO_RAD*(bearing_from_to(from, to));
    int R = 6371 * 1000; //radius of the world in meters
    return asin(sin(d13/R)*sin(brng13-brng12)) * R;
}


coord destination_from_distance_bearing(coord from, float d, float bearing)
{
    float R = 6371000.0; //radius of the world in meters

    float brng = bearing * DEG_TO_RAD;

    coord orig;
    orig.lat = from.lat*DEG_TO_RAD;
    orig.lng = from.lng*DEG_TO_RAD;

    coord dest;
    dest.lat = asin(sin(orig.lat)*cos(d/R) +
                    cos(orig.lat)*sin(d/R)*cos(brng));
    dest.lng = orig.lng + atan2(sin(brng)*sin(d/R)*cos(orig.lat),
                         cos(d/R)-sin(orig.lat)*sin(dest.lat));

/*
φ2 = asin( sin φ1 ⋅ cos δ + cos φ1 ⋅ sin δ ⋅ cos θ )
	λ2 = λ1 + atan2( sin θ ⋅ sin δ ⋅ cos φ1, cos δ − sin φ1 ⋅ sin φ2 )

	φ is latitude, λ is longitude, θ is the bearing (in radians, clockwise from north),
	δ is the angular distance (in radians) d/R; d being the distance travelled, R the earth’s radius
*/

    dest.lat *= RAD_TO_DEG;
    dest.lng *= RAD_TO_DEG;

    return dest;
}


