/*
    FireTail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ahrs.h"

#include "sensors/L3G.h"
#include "sensors/LSM303.h"
#include "hil.h"
#include "CompleteGPS.h"
#include "airdata.h"
#include "firetail.h" //for vd
#include "coms.h" //for sending debug messages


L3G l3g;
LSM303 lsm;

imu::Quaternion q;
imu::Quaternion offset;
imu::Quaternion body;

static imu::Vector<3> ahrs_integ;
static imu::Vector<3> ahrs_last_correction;

const static float ahrs_pbeta = 0.05;
const static float ahrs_ibeta = 0.0001;
const static float ahrs_dbeta = 0.001;

unsigned long last_micros = 0;

static float velocity;
static float lon_acceleration; //longitudinal acceleration
static unsigned long last_velocity = 0;

imu::Vector<3> raw_ang_vel;

static msg_t sensorThread(void *arg); //thread for I2C sensors

const int ahrs_stack_size = 4096;
static WORKING_AREA(ahrsStack, ahrs_stack_size);

static imu::Vector<3> ang_vel;
static imu::Vector<3> acceleration;
static imu::Vector<3> mag;

static imu::Vector<3> mag_bias;
static imu::Vector<3> mag_scale;
static imu::Vector<3> accel_bias;

PWMServoDriver servo_driver;

static msg_t sensorThread(void *arg)
{
    chThdSleepMilliseconds(4000); //pause 4 seconds before initialising AHRS.
    //this gives the user a chance to hold the aircraft stable before gyro bias is measured

    l3g.init();
    l3g.enableDefault();

    lsm.init(LSM303DLHC_DEVICE);
    lsm.enableDefault();

    chThdSleepMilliseconds(100);

    l3g.measureOffsets();

    chThdSleepMilliseconds(100);

    imu::Vector<3> acceleration = lsm.read_acc();
    imu::Vector<3> mag = lsm.read_mag();

    acceleration.x() *= -1.0f;
    mag.y() *= -1.0f;
    mag.z() *= -1.0f;


    mag = mag - mag_bias;

    mag.x() *= mag_scale.x();
    mag.y() *= mag_scale.y();
    mag.z() *= mag_scale.z();

    acceleration = acceleration - accel_bias;

    imu::Vector<3> down = acceleration;
    imu::Vector<3> east = down.cross(mag);
    imu::Vector<3> north = east.cross(down);

    down.normalize();
    east.normalize();
    north.normalize();

    imu::Matrix<3> m;
    m.vector_to_row(north, 0);
    m.vector_to_row(east, 1);
    m.vector_to_row(down, 2);

    q.fromMatrix(m);
    q.normalize();

    last_micros = us();

    ad_init();

    while(1)
    {
        ahrs_iterate();
        chThdYield();
    }

    return 0;
}

void ahrs_init()
{
    chThdCreateStatic(ahrsStack, sizeof(ahrsStack), NORMALPRIO, sensorThread, NULL);
}


void ahrs_set_offset(imu::Quaternion o)
{
    offset = o;
}

void ahrs_set_beta(float b)
{
    //beta = b;
}

void ahrs_set_velocity(float v)
{
    v *= KMH_TO_MS; //convert to m/s

    float dt;
    dt = ms() - last_velocity;
    last_velocity = ms();
    dt /= 1000.0;

    if(dt != 0)
        lon_acceleration = (velocity - v) / dt;

    velocity = v;
}

void ahrs_iterate()
{
    float dt;
    dt = us() - last_micros;
    if(dt < 5000) //constrain to 200hz
        return;

    last_micros = us();
    dt /= 1000000.0;

/*
    char buf[120];
    sprintf(buf, "%f", dt);
    coms.push_debug_message(buf);
*/

    if(!hil_mode_on)
    {
        ang_vel = l3g.read_gyros();
        acceleration = lsm.read_acc();
        mag = lsm.read_mag();

        //some of the raw sensor readings must be inverted
        //so that the quaternion math works 'as expected'
        ang_vel.z() *= -1.0f;
        ang_vel.y() *= -1.0f;

        acceleration.x() *= -1.0f;
        mag.y() *= -1.0f;
        mag.z() *= -1.0f;

        //update marg struct so that raw acceleration & magnetometer values are sent
        raw_marg.acc_x = acceleration.x()*1000;
        raw_marg.acc_y = acceleration.y()*1000;
        raw_marg.acc_z = acceleration.z()*1000;

        raw_marg.mag_x = mag.x();
        raw_marg.mag_y = mag.y();
        raw_marg.mag_z = mag.z();

        mag = mag - mag_bias;

        mag.x() *= mag_scale.x();
        mag.y() *= mag_scale.y();
        mag.z() *= mag_scale.z();

        acceleration = acceleration - accel_bias;
    }
    else
    {
        ang_vel = hil_gyro;
        acceleration = hil_acc*MSS_TO_G;
        mag = hil_mag;
    }

    acceleration = acceleration * G_TO_MSS;

    raw_ang_vel = ang_vel;

    imu::Vector<3> pseudo_gravity;
    //set some reasonable limitations for velocity/centrifugal force compensation
    if(velocity < 1000.0)
    {
        imu::Vector<3> centrifugal_force(imu::Vector<3>(velocity, 0, 0).cross(ang_vel));
        pseudo_gravity = acceleration - centrifugal_force;
    }
    else
        pseudo_gravity = acceleration;

#ifdef AHRS_NORMAL_COMPLEMENT
    imu::Vector<3> correction;
    imu::Vector<3> down = pseudo_gravity;
    imu::Vector<3> east = down.cross(mag);
    imu::Vector<3> north = east.cross(down);

    down.normalize();
    east.normalize();
    north.normalize();

    imu::Matrix<3> rotationMatrix = q.toMatrix();
    correction = (
                     north.cross(rotationMatrix.row_to_vector(0)) +
                     east.cross(rotationMatrix.row_to_vector(1)) +
                     down.cross(rotationMatrix.row_to_vector(2))
                 );

    imu::Vector<3> deriv = (correction - ahrs_last_correction)/dt;
    imu::Vector<3> w = ang_vel + (correction*ahrs_pbeta) + (ahrs_integ*ahrs_ibeta*dt) + (deriv*ahrs_dbeta);

    imu::Vector<3> new_ahrs_integ = ahrs_integ + correction;
    ahrs_integ = new_ahrs_integ;

    ahrs_last_correction = correction;

    double theta = w.magnitude() * dt;
    w.normalize();

    imu::Quaternion m;
    m.fromAxisAngle(w, theta);

    m.normalize();
    q = q * m;

#endif

#ifdef AHRS_ACCEL_ONLY
    imu::Vector<3> down = pseudo_gravity;
    imu::Vector<3> east = down.cross(mag);
    imu::Vector<3> north = east.cross(down);

    down.normalize();
    east.normalize();
    north.normalize();


    imu::Matrix<3> m;
    m.vector_to_row(north, 0);
    m.vector_to_row(east, 1);
    m.vector_to_row(down, 2);

    imu::Quaternion am;
    am.fromMatrix(m);

    q = am;
#endif

#ifdef AHRS_GYRO_ONLY
    imu::Vector<3> w;
    w = ang_vel;
    imu::Quaternion qm(1, w.x()*dt/2.0, w.y()*dt/2.0, w.z()*dt/2.0);
    qm.normalize();

    q = q * qm;
#endif

    body = q;// * offset.conjugate();

/*
    //One way to benchmark the accuracy of the AHRS is to see how quickly velocity drifts
    imu::Vector<3> wa = ahrs_world_acceleration();
    wa.z() -= 9.80665;
    ahrs_velocity += wa.magnitude();
    cout << ahrs_velocity << endl;
*/

    ad_iterate();


    //now write to servos
    servo_driver.write_vals();
}


imu::Vector<3> ahrs_get_euler()
{
    imu::Vector<3> euler = body.toEuler();
    euler.toDegrees();
    return euler;
}

imu::Matrix<3> ahrs_get_matrix()
{
    return body.toMatrix();
}

imu::Quaternion ahrs_get_quaternion()
{
    return body;
}

imu::Quaternion ahrs_get_imu_quaternion()
{
    return q;
}

void ahrs_set_mag_cal(imu::Vector<3> bias, imu::Vector<3> scale)
{
    mag_bias = bias;
    mag_scale = scale;
}

void ahrs_get_mag_cal(imu::Vector<3>* bias, imu::Vector<3>* scale)
{
    *bias = mag_bias;
    *scale = mag_scale;
}

void ahrs_set_accel_bias(imu::Vector<3> ab)
{
    accel_bias = ab;
}

imu::Vector<3> ahrs_get_accel_bias()
{
    return accel_bias;
}


imu::Vector<3> ahrs_get_mag()
{
    return lsm.m;
}

imu::Vector<3> ahrs_raw_ang_vel()
{
    return raw_ang_vel;
}


imu::Vector<3> ahrs_world_acceleration()
{
    if(hil_mode_on)
        return q.rotateVector(hil_acc);
    return q.rotateVector(acceleration);
}

