#ifndef FIRETAIL_CONTROL_H
#define FIRETAIL_CONTROL_H


#include "pid.h"
#include "fuzzy.h"


enum AUTOLAUNCH_STATE : uint8_t
{
    AUTOLAUNCH_PRELAUNCH, //waiting for launch to begin
    AUTOLAUNCH_DETECT, //acceleration detected, waiting for GPS speed to confirm
    AUTOLAUNCH_ROLLING, //now rolling down runway, waiting to reach rotation speed
    AUTOLAUNCH_ROTATE //rotation has occured, now climbing up to altitude
};



/**
----Control Functions----
These are the functions that control the aircraft.
They are named after their respective autopilot modes.
Control is achieved by cascading PID controllers. Pitch and roll control is the lowest level.
So all of these functions eventually make a call to the pitch_and_roll_control() function.

The autopilot mode defines the level of control. For example, in loiter mode, the loiter function is called.
The loiter function selects a heading and altitude, and then calls heading_and_alt_control(). The heading & alt
controllers then select a pitch and roll, and so on.
**/
void control_iteration(int16_t* ins, int16_t* outs, float dt);
void pitch_and_roll_control(int16_t* outs);
void heading_and_alt_control(int16_t* outs);
void orbit(int16_t* outs, bool clockwise);
void return_to_base(int16_t* outs);
void waypoints(int16_t* outs);

void autolaunch(int16_t* outs);

//this is what happens when GPS lock is lost during an autonomous mode
void fixed_bank_loiter(int16_t* outs);

int16_t autothrottle_with_pitch();
int16_t autothrottle_with_vert_speed();


extern PIDController roll_controller;
extern PIDController pitch_controller;
extern PIDController vs_controller;
extern PIDController alt_controller;
extern CircularPIDController heading_controller;
extern PIDController throttle_controller;
extern FuzzySet speed_set;

extern AUTOLAUNCH_STATE autolaunch_state;

#endif // FIRETAIL_CONTROL_H

