#include "pca_servo.h"

PWMServoDriver::PWMServoDriver(uint8_t addr)
{
    _i2caddr = addr;
}

void PWMServoDriver::begin(void)
{
    reset();
}


void PWMServoDriver::reset(void)
{
    write8(PCA9685_MODE1, 0x0);
}

void PWMServoDriver::set_outputs(int16_t* u)
{
    for(int i = 0; i < 6; i++)
        outputs[i] = (u[i]+128);
}

void PWMServoDriver::setPWMFreq(float freq)
{
    //Serial.print("Attempting to set freq ");
    //Serial.println(freq);
    freq *= 0.9;  // Correct for overshoot in the frequency setting (see issue #11).
    float prescaleval = 25000000;
    prescaleval /= 4096;
    prescaleval /= freq;
    prescaleval -= 1;
    uint8_t prescale = floor(prescaleval + 0.5);

    uint8_t oldmode = read8(PCA9685_MODE1);
    uint8_t newmode = (oldmode&0x7F) | 0x10; // sleep
    write8(PCA9685_MODE1, newmode); // go to sleep
    write8(PCA9685_PRESCALE, prescale); // set the prescaler
    write8(PCA9685_MODE1, oldmode);
    delay(5);
    write8(PCA9685_MODE1, oldmode | 0xa1);  //  This sets the MODE1 register to turn on auto increment.
    // This is why the beginTransmission below was not working.
    //  Serial.print("Mode now 0x"); Serial.println(read8(PCA9685_MODE1), HEX);
}

void PWMServoDriver::setPWM(uint8_t num, uint16_t on, uint16_t off)
{
    //Serial.print("Setting PWM "); Serial.print(num); Serial.print(": "); Serial.print(on); Serial.print("->"); Serial.println(off);

    Wire.beginTransmission(_i2caddr);
    Wire.write(LED0_ON_L+4*num);
    Wire.write(on);
    Wire.write(on>>8);
    Wire.write(off);
    Wire.write(off>>8);
    Wire.endTransmission();
}

// Sets pin without having to deal with on/off tick placement and properly handles
// a zero value as completely off.  Optional invert parameter supports inverting
// the pulse for sinking to ground.  Val should be a value from 0 to 4095 inclusive.
void PWMServoDriver::setPin(uint8_t num, uint16_t val, bool invert)
{
    // Clamp value between 0 and 4095 inclusive.
    val = min(val, 4095);
    if (invert)
    {
        if (val == 0)
        {
            // Special value for signal fully on.
            setPWM(num, 4096, 0);
        }
        else if (val == 4095)
        {
            // Special value for signal fully off.
            setPWM(num, 0, 4096);
        }
        else
        {
            setPWM(num, 0, 4095-val);
        }
    }
    else
    {
        if (val == 4095)
        {
            // Special value for signal fully on.
            setPWM(num, 4096, 0);
        }
        else if (val == 0)
        {
            // Special value for signal fully off.
            setPWM(num, 0, 4096);
        }
        else
        {
            setPWM(num, 0, val);
        }
    }
}

void PWMServoDriver::setServoPulse(uint8_t n, double pulse)
{
    double pulselength;
    pulselength = 1000000; // 1,000,000 us per second
    pulselength /= 60; // 60 Hz
    pulselength /= 4096; // 12 bits of resolution
    pulse *= 1000;
    pulse /= pulselength;
    setPWM(n, 0, pulse);
}


uint8_t PWMServoDriver::read8(uint8_t addr)
{
    Wire.beginTransmission(_i2caddr);
    Wire.write(addr);
    Wire.endTransmission();

    Wire.requestFrom((uint8_t)_i2caddr, (uint8_t)1);
    return Wire.read();
}

void PWMServoDriver::write8(uint8_t addr, uint8_t d)
{
    Wire.beginTransmission(_i2caddr);
    Wire.write(addr);
    Wire.write(d);
    Wire.endTransmission();
}

void PWMServoDriver::write_vals()
{
    for(int i = 0; i < 6; i++)
        setServoPulse(i, ((outputs[i]*3.90625)+1000)/1000.0);
}
