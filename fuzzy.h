#ifndef FUZZY_H_INCLUDED
#define FUZZY_H_INCLUDED

#include <stdint.h>

enum FUZZY_SET_POSITION : uint8_t
{
    FUZZY_SET_POS_START,
    FUZZY_SET_POS_MID,
    FUZZY_SET_POS_END
};


class FuzzyMember
{
public:
    void set_minmax(float p1, float p2) {
        min = p1;
        max = p2;
    }
    float get_dom(float crisp_in);
    void set_value(float v) {
        val = v;
    }

    float get_result(float crisp_in) {
        return get_dom(crisp_in)*val;
    }
    void set_position(FUZZY_SET_POSITION p) {
        pos = p;
    }
protected:
    float min, max;
    FUZZY_SET_POSITION pos;

    float val;
};

class FuzzySet
{
public:
    FuzzySet();

    FuzzyMember* get_set(int s) {
        return &sets[s];
    }

    float crisp_out(float crisp_in);

private:
    FuzzyMember sets[5];

};



#endif

