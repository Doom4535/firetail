/*
    Firetail UAV System
    Copyright (C) 2013-2014  Samuel Cowen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "logger.h"

Sd2Card sdc_card;
SdVolume sdc_volume;
SdFat sdc_fs;

bool sd_card_ok = false;

char log_name[128];

static msg_t loggerThread(void *arg); //thread for SD card logger
static WORKING_AREA(loggerStack, 2048);

bool save_waypoints = false;

waypoint* wp;
waypoint* gp;


int sd_card_init()
{
    pinMode(SD_CARD_SS_PIN, OUTPUT);

    if(!sdc_card.begin(SD_CARD_SS_PIN, SPI_FULL_SPEED))
        return -1;

    if (!sdc_volume.init(&sdc_card))
        return -2;

    sd_card_ok = true;

    if(!sdc_fs.begin(SD_CARD_SS_PIN, SPI_FULL_SPEED))
        return -1;

    return 1;
}


bool sd_card_start_logging()
{
    int counter = 0;
    while(counter < 1000)
    {
        //create a log name and assign it to 'log_name'
        snprintf(log_name, 128, "%04d.log", counter);

        //loop over every file in the working directory (root)
        ifstream fl;
        fl.open(log_name);
        if(!fl.is_open())
        {
            break;
        }
        fl.close();
        counter++;
    }
    if(counter >= 1000)
        return false;

    chThdCreateStatic(loggerStack, sizeof(loggerStack), NORMALPRIO, loggerThread, NULL);
    return true;
}

void sd_card_load_plan(waypoint* wp, waypoint* gp)
{
    ifstream fin;
    fin.open("wpoints");
    if(!fin.is_open())
        return;

    char ln[256];
    for(int i = 0; i < 512; i++)
    {
        if(fin.getline(ln, 256, '\n'))
        {
            sscanf(ln, "%f, %f, %i", &wp[i].pos.lat, &wp[i].pos.lng, &wp[i].alt);
            wp[i].used = true;
        }
        else
            break;
    }
    fin.close();

    fin = ifstream("gpoints", ios::in);
    if(!fin)
        return;

    for(int i = 0; i < 512; i++)
    {
        if(fin.getline(ln, 256, '\n'))
        {
            sscanf(ln, "%f, %f", &gp[i].pos.lat, &gp[i].pos.lng);
            gp[i].used = true;
        }
        else
            break;
    }
    fin.close();
}

void sd_card_save_plan(waypoint* _wp, waypoint* _gp)
{
    wp = _wp; gp = _gp;
    save_waypoints = true;
}

void sd_card_save_waypoints(waypoint* wp, waypoint* gp)
{
    ofstream fout("wpoints", ios::out | ios::trunc);
    if(!fout)
        return;

    char ln[256];
    int i = 0;
    while(wp[i].used)
    {
        sprintf(ln, "%0.6f, %0.6f, %i\n", wp[i].pos.lat, wp[i].pos.lng, wp[i].alt);
        fout << ln;

        i++;
        if(i > 512)
            break;
    }
    fout.close();


    fout = ofstream("gpoints", ios::out | ios::trunc);
    if(!fout)
        return;

	i = 0;
    while(gp[i].used)
    {
        sprintf(ln, "%0.6f, %0.6f\n", gp[i].pos.lat, gp[i].pos.lng);
        fout << ln;

        i++;
        if(i > 512)
            break;
    }
    fout.close();
}

static msg_t loggerThread(void *arg)
{
    unsigned long last_ms = ms();
    while(1)
    {
        //voluntarily relinquish CPU time, so more important threads can run
        chThdYield();

        if(save_waypoints)
        {
            sd_card_save_waypoints(wp, gp);
            save_waypoints = false;
        }

        //constrain rate to 5Hz, or once per 200ms
        if((ms() - last_ms) < 200)
            continue;

        last_ms = ms();

        //open the log file, using the append flag
        ofstream sdout(log_name, ios::out | ios::app);
        if(!sdout)
            continue;

        //TODO somehow allow users to choose which parameters to log, rather than have it hard coded
        //there is no euler.x() - gps course is logged instead
        imu::Vector<3> euler = ahrs_get_euler();
        sdout << gps.latitude << ", " << gps.longitude << ", " << gps.speed << ", "
              << gps.course << ", " << euler.y() << ", " << euler.z() << ", "
              << ad_pressure_alt() << ", " << ad_vert_speed() << ", " << ad_temp() << "\n";
        sdout.flush();
        sdout.close();
    }
    return 0;
}

